import { createGlobalStyle } from 'styled-components';

export const theme = {
  color: {
    bgBlue: '#1A1830',
    bgDark: 'rgba(33, 33, 33, 1)',
    grey100: 'rgba(30, 29, 50, 1)',
    white100: 'rgba(255, 255, 255, 1.0)',
    white70: 'rgba(255, 255, 255, 0.7)',
    white10: 'rgba(255, 255, 255, 0.10)',
    orange100: '#E9702A',
    orangedark100: 'rgba(180,85,35,1)',
    turquoise100: '#0F8EA2',
    pink100: '#E3398B',
    darkpink100: 'rgba(107, 33, 74, 1)',

    // 2023 colors
    bgGreen: 'rgba(2, 51, 53, 1)',
    lightOrange: 'rgba(249, 171, 128)',
    realOrange: 'rgba(216, 106, 44, 1)',
    darkOrange: 'rgba(180,85,35,1)',
    white2: 'rgba(253, 244, 224, 1)',

  },
  font: {
    gothic: 'Gothic A1, sans-serif', // Similar to League Gothic, serif headings.
    ptsans: 'PT Sans, sans-serif', // Similar to Myriad Pro, body text.
  }
};

export const GlobalStyle = createGlobalStyle`
    body {
      font-family: ${theme.font.ptsans};
      letter-spacing: 0.5px;
      max-width: 1000px;
      margin: 0 auto 0;
      padding: 1rem;
      line-height: 1.5rem;
      overflow-x: hidden;
      position: relative;
    }

    html {
      box-sizing: border-box;
      background-color: ${theme.color.bgDark};
      color: ${theme.color.white2};
    }

    *,
    *::before,
    *::after {
      box-sizing: inherit;
    }

    h1,
    h2 {
      line-height: 2rem;
      font-size: 1.5rem;
      font-family: ${theme.font.gothic};
      font-weight: bolder;
      color: ${theme.color.white100};
      display: flex;
      margin: 0;
      padding: 1.5rem 0 0;
    }

    h3 {
      font-size: 1.5rem;
      color: ${theme.color.white100};
    }

    a {
      color: ${theme.color.lightOrange};
      text-decoration: none;
      cursor: pointer;

      &:hover {
        text-decoration: underline;
      }
    }

    .span-anchor {
      color: ${theme.color.lightOrange};
      text-decoration: none;
      cursor: pointer;

      &:hover {
        text-decoration: underline;
      }
    }
`;

export default GlobalStyle;
