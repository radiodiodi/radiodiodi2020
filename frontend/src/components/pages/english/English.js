import React, { Component, Fragment } from 'react';
import styled from 'styled-components';

const Quote = styled.p`
  text-align: right;
  margin-right: 4rem;
  font-style: italic;
`;

const Source = styled.p`
  text-align: right;
  margin-right: 1rem;
`;

const Paragraph = styled.p`
  text-align: ${p => p.centered ? 'center' : 'left'};
`;

const Title = styled.h2`
  text-align: left;
`;

class English extends Component {
  render() {
    return (
      <Fragment>
        <Quote>The cheapest student radio in downtown Otaniemi!</Quote>
        <Source>— Radiodiodi</Source>

        <Title>Student Radio from Otaniemi</Title>
        <Paragraph>
          Radiodiodi is a temporary student radio station based in Otaniemi.
          Our main goal is to organize a two-week-long radio station project from the middle of April until the festive Wappu and a shorter production for orientation in the beginning of September.
          The broadcast is produced in Otaniemi campus in a construction container town.
        </Paragraph>
        <Paragraph>
          Programmes are produced by volunteering Aalto students (even you!).
          The Radiodiodi team has no clue about the content of the broadcast &ndash; we facilitate it, you make it.
        </Paragraph>
        <Paragraph>
          We organize Radiodiodi for the community and we hope to make a great Wappu for everyone!
        </Paragraph>

        <Title>Reserving a programme slot</Title>
        <Paragraph>
          If you're able to use the Finnish registration form, go ahead. If not,
          send an email to <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi</a> to reserve a slot.
          Provide a name and description for your programme. Also specify if you need a producer and when you would be able to host the programme.
          The reservation is later confirmed by the editorial board of Radiodiodi.
        </Paragraph>

        <Title>Language</Title>
        <Paragraph>
          The primary language of the project is Finnish, but we have no qualms about setting up an international programme for you.
          If you need more information about the project in English, please contact us at <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi</a>.
        </Paragraph>
      </Fragment>
    );
  }
}

export default English;
