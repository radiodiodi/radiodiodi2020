import React, { Component } from 'react';
import styled from 'styled-components';

import { parseSearch } from '../../../utils';

const Container = styled.div`
  margin-bottom: 2rem;
`;

const Paragraph = styled.div`
  max-width: 700px;
  margin-bottom: 1rem;
  flex-grow: 1;

  hr {
    width: 50%;
    text-align: left;
  }
`;

const Title = styled.h3`
  margin: 0;
  height: 4rem;
`;

class RegistrationSuccess extends Component {
  render() {
    const { location } = this.props;
    const { search } = location;
    const queryParams = parseSearch(search);
    const { email } = queryParams;

    return (
      <Container>
        <Paragraph>
          <Title>Ilmoittautuminen onnistui!</Title>
          <Paragraph>
            Kiitos ilmoittautumisesta! Olemme yhteydessä lähiaikoina.
          </Paragraph>
          { email && (
          <Paragraph>
            Vahvistus ilmoittautumisesta on lähetetty sähköpostiosoitteeseen { email }.
          </Paragraph>
          )}
          {/* <Paragraph>
            Ohjelmantekijät kuvataan 3.4. ja 4.4. myöhemmin ilmoitettavassa paikassa. Jos jäi kysymyksiä, vastauksia löytyy postilokerosta <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi.</a>
          </Paragraph> */}
        </Paragraph>
      </Container>
    );
  }
}

export default RegistrationSuccess;
