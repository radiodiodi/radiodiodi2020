import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

// import RegistrationForm from './RegistrationForm';
import { linkToTelegramChat } from '../../../utils';

const Main = styled.main`
  p {
    max-width: 700px;
  }
`;

const Registration = () => {
  return (
    <Main>
      <h1>Ilmoittaudu radioon!</h1>
      <p>
        Ilmoittaudu tekemään radio-ohjelmaa Wapuksi 2022! Ohjelmaa laitetaan kohti Internettiä ja FM-radiota 14.4.-30.4. Radiodiodin ohjelma on yhteisön tuottamaa. 
        Kuka tahansa voi ilmoittautua mukaan tekemään ohjelmaa. Kannustamme monipuolisuuteen ja luovaan hulluuteen. Ohjelman tekeminen ja kaikki siihen liittyvä
        oheistoiminta on opiskelijoille ilmaista.
      </p>
      <p>
        Kevään radio järjestetään ennen Wappua (14.-30.4.). Radioon pääsee livenä tahi ohjelman voi äänittää etukäteen. Tarkemmista käytännön järjestelyistä tiedotamme mm. radion Telegram-kanavilla{' '}
        {/* eslint-disable jsx-a11y/anchor-is-valid */}
        <a title="Telegram-tiedotus" onClick={linkToTelegramChat('radiodiodi')}>tiedotus</a>{' '}
        ja <a title="Telegram-keskustelu" onClick={linkToTelegramChat('radiodiodichat')}>keskustelu</a>.
        {/* eslint-enable jsx-a11y/anchor-is-valid */}
      </p>
      <p>
        Ohjelmavuorot ovat yleensä tunnin tai parin mittaisia. Ohjelmaan sisältyy tyypillisesti puhetta, musiikkia sekä Radiodiodin tuottamia sponsorien mainoksia, mutta puitteet sallivat myös esimerkiksi livemusiikin esittämisen.
        Suunnittelethan ohjelmasi huolella! Tehokasta apua suunnitteluun löydät <Link to="/guide/checklist">ohjelmantekijän checklististä </Link>
        sekä <Link to="/guide/faq">usein kysytyistä kysymyksistä</Link>.
        Kannattaa käydä myös tutustumassa Radiodiodin <Link to="/library">musiikkikirjastoon</Link>.
      </p>
      <p>
        Jos sinulla on kysymyksiä Radiodiodiin tai ohjelman tekemiseen liittyen, ota yhteyttä toimitukseen <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi</a>
      </p>

      <h3>Ilmoittautuminen ohjelmantekijäksi on kiinni!</h3>
      {/* <p>Tarkistathan ohjelmakalenterista vapaat ajat ennen ilmoittautumista!</p> */}
      {/* <RegistrationForm /> */}
    </Main>
  );
}

export default Registration;
