import React, { Component } from 'react';
import { withRouter } from "react-router";
import axios from 'axios';
import {
  Form as InformedForm,
  Text as InformedText,
  TextArea as InformedTextArea,
  RadioGroup as InformedRadioGroup,
  Radio,
  asField,
} from 'informed';
import styled from 'styled-components';
import PropositionPicker from '../../common/PropositionPicker';
import { environment } from '../../../utils';

const BACKEND_URL = environment.REACT_APP_BACKEND_HTTP_URL;

const Form = styled(InformedForm)`
  margin: 3rem 0;

  input,
  text,
  textarea {
    font-family: inherit;
    font-size: 1rem;
    border: 3px inset ${p => p.theme.color.realOrange};
  }

  p {
    max-width: 70ch;
  }

  fieldset {
    border: none;
    border-top: 0.2rem solid ${p => p.theme.color.realOrange};
    margin-top: 2rem;
  }

  legend {
    font-size: 1.2rem;
    padding: 0 0.5em;
  }

  strong {
    color: #E3398B;
  }

`;

const Fieldset = styled.fieldset`
  @media screen and (min-width: 800px) {
    label {
      display: flex;
      justify-content: space-between;
    }
  }
`

const CustomNumberField = asField(({ fieldState, fieldApi, ...props }) => {
  const { value } = fieldState;
  const { setValue, setTouched } = fieldApi;
  const { onChange, onBlur, initialValue, forwardedRef, ...rest } = props;
  return (
    <input
      {...rest}
      type="number"
      ref={forwardedRef}
      step={0.5}
      min={1}
      max={10}
      value={value || initialValue}
      onChange={e => {
        setValue(e.target.value);
        if (onChange) {
          onChange(e);
        }
      }}
      onBlur={e => {
        setTouched(true);
        if (onBlur) {
          onBlur(e);
        }
      }}
      />
  );
});

const NumberField = styled(CustomNumberField)`
  padding: 0.5rem;
  max-width: 500px;
  width: 100%;

`;

const StyledText = styled(InformedText)`
  padding: 0.5rem;
  max-width: 500px;
  width: 100%;
`;

const StyledTextArea = styled(InformedTextArea)`
  padding: 0.5rem;
  max-width: 500px;
  width: 100%;
`;

const Text = (props) => (
  <StyledText validate={notEmptyString} {...props} />
);

const TextArea = (props) => (
  <StyledTextArea validate={!props.optional && notEmptyString} {...props} />
);

const RadioGroup = (props) => (
  <InformedRadioGroup validate={notEmptyString} {...props} />
);

const Label = styled.label`
  display: block;
  margin: 1em 0;

  input:not([type="checkbox"]):not([type="radio"]),
  textarea {
    display: block;
  }

  input[type="radio"],
  input[type="checkbox"] {
    margin: 0 1rem;
  }
`;

const Submit = styled.button`
  display: block;
  width: 7em;
  padding: 1em 1.5em;
  margin: 2em auto;
  background-color: ${p => p.theme.color.realOrange};
  font-size: 1.1rem;
  color: ${p => p.theme.color.white100};

  border: none;
  outline: none;
  cursor: pointer;

  &:hover,
  &:focus {
    background-color: ${p => p.theme.color.darkOrange};
  }
`;

const Info = styled.p`
  margin-left: 2em;
  /* color: ${p => p.theme.color.realOrange}; */
  color: #bbb;
`

const Error = styled.div`
  color: red;
`;

const Tag = styled.span`
  color: ${p => p.theme.color.realOrange}
`;

const notEmptyString = value => !value ? 'Kenttää ei voi jättää tyhjäksi' : undefined;
const validatePropositions = propositions => {
  if (!propositions || propositions.length === 0) {
    return 'Ehdota vähintään yhtä aikaväliä';
  }

  for (const p of propositions) {
    if (!p.date) {
      return 'Ainakin yhden aikaehdotuksen päivämäärä on asettamatta';
    }
    if (!p.startTime) {
      return 'Ainakin yhden aikaehdotuksen aloituskellonaika on asettamatta';
    }
    if (!p.endTime) {
      return 'Ainakin yhden aikaehdotuksen lopetuskellonaika on asettamatta';
    }
  };

  return undefined;
}

class RegistrationForm extends Component {
  setFormApi = (formApi) => {
    this.formApi = formApi;
  }

  handleSubmit = async () => {
    const { history } = this.props;
    const state = this.formApi.getState();
    console.log(state);

    try {
      if (state.values.prerecord === 'live') {
        // remove null propositions
        state.values.propositions = state.values.propositions.filter(p => p.date);
      }
      const resp = await axios.post(`${BACKEND_URL}/api/register`, state.values);
      console.log(resp);
      const email = state.values.email;
      history.push(`/ilmo/success?email=${email}`);
    } catch (err) {
      console.error(err);
      history.push('/ilmo/error');
    }
  }

  getErrors = () => this.formApi
    ? this.formApi.getState().errors || {}
    : {};

  getValues = () => this.formApi
    ? this.formApi.getState().values || {}
    : {};

  renderErrors = () => {
    const errors = this.getErrors();
    const hasErrors = !!Object.keys(errors).length;
    if (!hasErrors) return null;

    return (
      <Error>
        Lomakkeessa on virheitä. Tarkista vielä kenttien tiedot!
      </Error>
    )
  }

  rerender = () => {
    // force rerender
    this.setState({});
  }

  handleSubmitFailure = (data) => {
    this.rerender();
    return true;
  }

  setPropositionValues = (value) => {
    const formApi = this.formApi;
    if (formApi) formApi.setValue('propositions', value);
  }

  getPropositionValues = () => {
    const formApi = this.formApi;
    const val = formApi ? formApi.getValue('propositions') : [];
    return val || [];
  }

  render() {
    const errors = this.getErrors();
    const values = this.getValues();

    return (
      <Form getApi={this.setFormApi}
        onSubmit={this.handleSubmit}
        onSubmitFailure={this.handleSubmitFailure}
        initialValues={{
          propositions: [],
        }}>
        <Fieldset>
          <legend>Ohjelman tiedot</legend>
          <Label>
            Ohjelman nimi
            {errors.name && <Error>{errors.name}</Error>}
            <Text
              field="name"
              placeholder="Rätinää ja rällätystä" />
          </Label>
          <Label>
            Ohjelman kuvaus
            {errors.description && <Error>{errors.description}</Error>}
            <TextArea
              field="description"
              placeholder="Miten markkinoisit ohjelmaasi radion kuuntelijoille? Tämä tulee näkyviin ohjelmakalenteriin sen julkaisun jälkeen. Maksimissaan 400 merkkiä."
              rows="5"
              maxlength="400"
            />
          </Label>
          <Label>
            Ohjelmantekijän tai tiimin nimi
            {errors.team && <Error>{errors.team}</Error>}
            <Text
              field="team"
              placeholder="Ossi Ohjelmantekijä ja negatiiviset nopat" />
          </Label>
          <Label>
            Vastuuhenkilö
            {errors.responsible && <Error>{errors.responsible}</Error>}
            <Text
              field="responsible"
              placeholder="Oma nimi"
            />
          </Label>
          <Label>
            Vastuuhenkilön sähköpostiosoite
            {errors.email && <Error>{errors.email}</Error>}
            <Text
              field="email"
              type="email"
              placeholder="email@example.com"
            />
          </Label>
          <Label>
            Ohjelman tyyppi
            {errors.genre && <Error>{errors.genre}</Error>}
            <Text
              field="genre"
              placeholder="Huumori ja keskustelu" />
          </Label>
          <Label>
            Osallistujien (arvioitu) määrä
            {errors.participants && <Error>{errors.participants}</Error>}
            <Text
              field="participants"
              type="number"
              min={1}
              max={5}
              placeholder="Studioon mahtuu enintään 4 ja tuottaja." />
            {/* <p>Jos ohjelmaasi on aikeissa tulla suuri joukko ihmisiä, keskustele ensin Radiodiodin toimituksen kanssa.</p> */}
          </Label>
          <Label>
            Ohjelman kesto (tunneissa) 
            {errors.duration && <Error>{errors.duration}</Error>}
            <NumberField field="duration" placeholder="1.5 (1h 30min)" />
          </Label>
          <Info>
            Ohjelmat alkavat aina tasalta tai puolelta ja päättyvät varttia vaille tai yli.
            Studiosta pitää poistua viimeistään 10 min ennen seuraavan ohjelman alkua.
            <br/>
            Tarkista ohjelmasi keston järkevyys <a target="_blank" href="/guide/checklist#calculator">ohjelmalaskurilla</a>!
          </Info>
          <Label>
            Lisätietoja
            {errors.info && <Error>{errors.info}</Error>}
            <TextArea
              field="info"
              placeholder="Kysymyksiä toimitukselle? Erityisiä toiveita lähetykseen liittyen? Muita toiveita? Tänne vaan."
              optional="true"
              rows="4"
            />
          </Label>
        </Fieldset>
        <fieldset>
          <legend>Tuottaja</legend>
          <Label>
            <p>Studiossa on ohjelman esiintyjien lisäksi aina tuottaja, joka vastaa lähetysteknisistä asioista (esim. musiikin ja mainosten soittaminen). Radio kouluttaa myös kiinnostuneita tuottajia ohjelmantekijöistä, joten oman ohjelman tuottaminen itse on mahdollista.</p>
            {errors.producer && <Error>{errors.producer}</Error>}
            <RadioGroup
              field="producer"
              onValueChange={this.rerender}>
              <Label>
                <Radio value="ei omaa tuottajaa" />
                Tarvitsen tuottajan
              </Label>
              <Label>
                <Radio value="oma tuottaja" />
                Minulla on tuottaja, joka aikoo käydä tuottajakoulutuksessa
              </Label>
              {values.producer === 'oma tuottaja' && (
                <Info>Tuottajien tulee käydä Radiodiodin studiotiimin järjestämässä tuottajakoulutuksessa.
                  Tuottajakoulutuksien päivämäärät kerrotaan myöhemmin.
                  Laita viestiä osoitteeseen <a href="mailto:studio@radiodiodi.fi">studio@radiodiodi.fi</a> ja liity
                  virallisille Telegram-kanaville, jottet missaa tärkeää tiedotusta!</Info>
              )}
            </RadioGroup>
          </Label>
        </fieldset>
        <fieldset>
          <legend>Nauhoitettu ohjelma?</legend>
          <Label>
            <p>Voimme toistaa myös ennalta nauhoitettuja ohjelmia. Voit siis nauhoittaa ohjelman itse, mutta äänenlaadun täytyy olla suhteellisen korkea, jotta se kelpaa lähetykseen. Radiodiodin tuottaja huolehtii musiikin, mainoksien ja jinglejen sisällyttämisestä ohjelmaan.</p>
            {errors.prerecord && <Error>{errors.prerecord}</Error>}
            <RadioGroup
              field="prerecord"
              onValueChange={this.rerender}>
              <Label>
                <Radio value="live" />
                Teen ohjelman livenä
              </Label>
              <Label>
                <Radio value="oma" />
                Teen oman nauhoituksen etukäteen
              </Label>
            </RadioGroup>
          </Label>
        </fieldset>
        {values.prerecord === 'live' &&
        <fieldset>
          <legend>Aikataulu</legend>
          <p>Valitse alta kaikki sopivat aikavälit ohjelmallesi. Aikavälit eivät merkkaa ohjelmasi kestoa, vaan sitä, milloin voit pitää ohjelmasi. </p>
          <strong>Varmista, että valitsemasi aikaväli on vapaana ohjelmakalenterissa!</strong>
          <p>
            Mikäli haluat pitää ohjelmaa yöaikaan ota yhteyttä <a href="mailto:toimitus@radiodiodi.fi">toimitukseen</a>.
          </p>
          {/* <p>Jälki-ilmoittautumisen yhteydessä emme pysty takaamaan sinulle parhaiten sopivaa aikaa. Kannattaa tarkistaa vapaat ajat ohjelmakalenterista, jota päivitetään alkuviikosta.</p> */}
          {errors.propositions && <Error>{errors.propositions}</Error>}
          <PropositionPicker
            field="propositions"
            validate={validatePropositions}
            getValues={this.getPropositionValues}
            setValues={this.setPropositionValues}
          />
        </fieldset>
        }
        <fieldset>
          <legend>Kuva</legend>
          <p>
            Halutessasi voit lähettää ohjelmantekijäkuvasi
            ohjelmakalenteria varten osoitteeseen <a href="mailto:kuvat@radiodiodi.fi">kuvat@radiodiodi.fi</a>.
            Kuvan tulee toimia kuvasuhteessa 1:1.
          </p>
          {/* <Label>
            Valokuvausaika
            {errors.photoshoot && <Error>{errors.photoshoot}</Error>}
            <RadioGroup
              field="photoshoot"
              onValueChange={this.rerender}>
              <Label>
                <Radio value="3.4." />
                3.4. klo 18
              </Label>
              <Label>
                <Radio value="4.4." />
                4.4. klo 18
              </Label>
              <Label>
                <Radio value="oma kuva" />
                Kuvaan julkaisukelpoisen kuvan itse
              </Label>
              {values.photoshoot === 'oma kuva' && (
                <Info>Jos haluat kuvata oman kuvasi, varmista, että se toimii myös 1:1-kuvasuhteessa (neliö) ja että se on korkealaatuinen valotuksen sekä kuvanlaadun osalta.</Info>
              )}
              <Label>
                <Radio value="mikään ei sovi" />
                Valitettavasti mikään vaihtoehto ei sovi
              </Label>
              {values.photoshoot === 'mikään ei sovi' && (
                <Info>Harmin paikka! Käytämme ohjelmasi kuvana oletusgrafiikkaa.</Info>
              )}
            </RadioGroup>
          </Label> */}
        </fieldset>
        <fieldset>
          <p>Jos jäi kysymyksiä, vastauksia löytyy postilokerosta <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi.</a></p>
          <p>
            Muista myös mainostaa omaa ohjelmaasi somessa! Voit käyttää tunnisteita <Tag>#radiodiodi</Tag> ja <Tag>@radiodiodi</Tag> tai linkata nettisivullemme <a href="https://radiodiodi.fi">radiodiodi.fi</a>.
          </p>
          {this.renderErrors()}
          <br />
          <Info style={{textAlign: "center", margin: "auto"}}>Lähettämällä lomakkeen hyväksyt, että annettuja tietoja säilytetään
          projektivuoden loppuun ja käytetään sähköpostiviestintään projektivuoden aikana.
          </Info>
          <Submit type="submit">Lähetä</Submit>
        </fieldset>
      </Form>
    );
  }
}

export default withRouter(RegistrationForm);
