import React, { Component } from 'react';
import styled from 'styled-components';

import Countdown from '../../common/Countdown';

const SectionContainer = styled.div`
  padding: 400px 0 400px;
  text-align: center;
`;

const H1 = styled.h1`
  font-size: 2rem;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 3rem;

  display: block;
`

const MonospaceSpan = styled.span`
  width: 15px;
  display: inline-block;
  font-size: 5rem;
  font-weight: bold;
  margin: 1rem;
`;

class RegistrationCounter extends Component {
  toMonospace(text) {
    return text
      .split('')
      .map((letter, i) => <MonospaceSpan key={i}>{letter}</MonospaceSpan>);
  }

  render() {
    return (
      <SectionContainer>
        <H1>Ohjelmantekijäilmoittautuminen aukeaa: </H1>
        <Countdown
          style={{ margin: '4rem' }}
          countTo={'Fri Feb 28 2020 12:00:00 GMT+0200 (EET)'}
          interval={1000}
          contentTransformFn={this.toMonospace}
        />
      </SectionContainer>
    );
  }
}

export default RegistrationCounter;
