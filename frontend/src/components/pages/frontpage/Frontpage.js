import React from 'react';
import { Link as ReactLink } from 'react-router-dom';
import styled from 'styled-components';

//import ImageGallery from '../../common/ImageGallery';
import Instagram from '../../common/Instagram';
// import Calendar from '../../calendar/Calendar';
// import Shoutbox from '../../common/Shoutbox';
import { linkToTelegramChat } from '../../../utils';
import VideoPlayer from '../../common/VideoPlayer';


const Container = styled.div`
  display: flex;
  gap: 1rem;
  margin-bottom: 2rem;

  @media screen and (max-width: 800px) {
    flex-direction: column-reverse;
    flex-direction: column;
  }

  & > * {
    /*flex: 0 1 50%;*/
  }
`;

const Title = styled.h1`
  margin: 0 0 1rem;
`;

const Paragraph = styled.div`
  margin-bottom: 1rem;
`;

const Trivia = styled.ul`
  li {
    margin-top: 1rem;
  }

  li::marker {
    color: ${p => p.theme.color.realOrange};
    font-size: 1.3em;
  }

  strong {
    color: ${p => p.theme.color.realOrange};
  }
`;

/* const Announcement = styled.div`
   padding: 2rem;
   background-color: ${p => p.theme.color.white10};
   font-size: 1.6rem;
   line-height: 1.1em;
   font-weight: bold;
   text-align: center;

   hr {
    width: 10%;
    border-style: dashed;
   }

   @media screen and (max-width: 360px) {
     font-size: 1.2rem;
     padding: 1rem;
}
`; */

const Recruitment = styled.div`
   padding: 2rem;
   background-color: ${p => p.theme.color.white10};
   font-size: 0.8rem;
   line-height: 0.5rem;
   font-weight: bold;
   text-align: center;
   font-style: italic;

   hr {
    width: 10%;
    border-style: dashed;
   }
`;

const Link = styled(ReactLink)`
   color: ${p => p.color || p.theme.color.lightOrange};
   transition: color 0.2s ease;

   &:hover, &:focus {
     color: ${p => p.theme.color.realOrange};
     text-decoration: none;
   }
 `;

// const CenteredParagraph = styled.section`
//   margin: 0 auto 3rem;
//   font-size: 1.2rem;
//   max-width: 80ch;
//   text-align: justify;
// `;


const Frontpage = () => {
  return (
    <>
{/*      <Announcement>
        Radiodiodi on mukana juhlistamassa 150-vuotista teekkariutta ja järjestää 6.-18. marraskuuta perinneviikkoradion.
        Lisäinfoa tulossa myöhemmin.
     </Announcement> */}
     <Recruitment>
       <Link to="/rekry">Mukaan toimittamaan Radiodiodia?</Link>
     </Recruitment>
      
      <Container>
        <div>
          <Paragraph>
            <Title>Mikä on Radiodiodi?</Title>
            <div>
            Radiodiodi on Otaniemestä ponnistava täysin opiskelijavetoisesti toteutettu radiokanava.
            Radiota on toteutettu vuodesta 2012 alkaen ja lähetys on käynnissä aina Wappuisin noin kahden viikon ajan huhtikuun loppupuolella.
            Radiota voi kuunnella lähetyskauden aikana suoraan verkosta tai radiovastaanottimestasi. Radio-ohjelmia voi tehdä kuka tahansa ja vuosittain ohjelmantekijöitä
            on yli 500! Projekti toteutetaan täysin vapaaehtoisvoimin ja taustatyöhön osallistuu kymmenittäin vapaaehtoisia. Radiodiodi etsii jatkuvasti innokkaita
            mahdollistamaan tätä projektia! Tee ite paremmin ja <Link to="/rekry">hae mukaan!</Link>
            </div>
            <br />
            <div>
              Lisätietoa Radiodiodista sekä antoisaa keskustelua löytyy
              Telegram-kanaviltamme&nbsp;
              <span
                className="span-anchor"
                title="Telegram-tiedotus"
                onClick={linkToTelegramChat('radiodiodi')}
              >
                tiedotus
              </span>{' '}
              sekä{' '}
              <span
                className="span-anchor"
                title="Telegram-keskustelu"
                onClick={linkToTelegramChat('radiodiodichat')}
              >
                keskustelu
              </span>
              .
            </div>
          </Paragraph>
          <Title>Tiesitkö?</Title>
          <Trivia >
            <li>Radiodiodi palkittiin Hugo-gaalassa 2018 Aallon&nbsp;
              <a href="https://www.ayy.fi/fi/hugo-gaala-palkitsi-aalto-yhteison-pioneereja">parhaana yhteisönrakentajana</a>
            </li>
            <li>Vuonna 2023 tuotettiin <strong>141</strong>&nbsp;radio-ohjelmaa</li>
            <li>"Lootusasentoon" soi lähetyskauden aikana <strong>61</strong>&nbsp;kertaa</li>
            <li>Skotlannin kansalliseläin on yksisarvinen</li>
          </Trivia>
          <Title>Ota yhteyttä</Title>
          <strong>Toimitus: </strong>
          <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi</a>
          <br />
          <strong>Yritysyhteistyö: </strong>
          <a href="mailto:yrityssuhteet@radiodiodi.fi">
            yrityssuhteet@radiodiodi.fi
          </a>
        </div>
          
      </Container>
      {/* <Shoutbox /> */}
      {/* <Calendar /> */}
      {/* <Title>Toimitus 2022</Title>
      <ImageGallery /> */}
      <Instagram />
      <VideoPlayer src="https://www.youtube-nocookie.com/embed/uublsUz-7Bw" />
    </>
  );
}

export default Frontpage;
