import React, { Component, Fragment } from 'react';
import axios from 'axios';
import Message from '../../admin/Message';
import MessagePanel from '../../admin/MessagePanel';
import Cookie from 'universal-cookie';
import AdminNavbar from '../../admin/AdminNavbar';
import { Count, Column, Container, Log } from '../../admin/Styles';
import { environment } from '../../../utils';

const cookie = new Cookie();

const BACKEND_URL = environment.REACT_APP_BACKEND_HTTP_URL;

const BannedWordsPanel = ({
  words,
  field,
  handleChange,
  handleSubmit,
  handleDelete
}) => {
  let wordRows;
  if (words.length === 0) {
    wordRows = [<div key="pirkka">No banned words</div>];
  } else {
    wordRows = words.map(word => {
      return (
        <li key={words._id}>
          {word.word} <button onClick={handleDelete(word)}>Delete</button>
        </li>
      );
    });
  }

  return (
    <Fragment>
      <ul>{wordRows}</ul>
      <form onSubmit={handleSubmit}>
        <label>
          Ban word:
          <input type="text" value={field} onChange={handleChange} />
        </label>
        <input type="submit" value="BAN!" />
      </form>
    </Fragment>
  );
};

class AdminShoutboxPage extends Component {
  state = {
    messages: [],
    selected: null,
    loading: true,
    logScrolled: false,
    filters: [],
    words: [],
    wordField: ''
  };
  logRef = React.createRef();

  fetchWords = async () => {
    const { history } = this.props;
    const token = cookie.get('jwt');
    try {
      const res = await axios.get(`${BACKEND_URL}/admin/words`, {
        headers: {
          Authorization: token
        }
      });
      const words = res.data.words;
      this.setState({
        words,
        loading: false
      });
    } catch (err) {
      console.error(err);
      if (err.response && err.response.status === 401) {
        cookie.remove('jwt');
        history.push('/login?next=/admin/shoutbox');
        return;
      }

      this.setState({
        loading: false,
        error: 'Error loading words, check console'
      });
    }
  };

  fetchFilters = async () => {
    const { history } = this.props;
    const token = cookie.get('jwt');
    try {
      const res = await axios.get(`${BACKEND_URL}/admin/filters`, {
        headers: {
          Authorization: token
        }
      });
      const filters = res.data.filters;
      this.setState({
        filters,
        loading: false
      });
    } catch (err) {
      console.error(err);
      if (err.response && err.response.status === 401) {
        cookie.remove('jwt');
        history.push('/login?next=/admin/shoutbox');
        return;
      }

      this.setState({
        loading: false,
        error: 'Error loading filters, check console'
      });
    }
  };

  fetchMessages = async () => {
    const { history } = this.props;
    const token = cookie.get('jwt');

    try {
      const resp = await axios.get(`${BACKEND_URL}/admin/messages`, {
        headers: {
          Authorization: token
        }
      });
      const messages = resp.data.messages;
      this.setState({
        messages,
        loading: false
      });
    } catch (err) {
      console.error(err);
      if (err.response && err.response.status === 401) {
        cookie.remove('jwt');
        history.push('/login?next=/admin/shoutbox');
        return;
      }

      this.setState({
        loading: false,
        error: 'Error loading messages, check console'
      });
    }
  };

  componentDidMount() {
    this.refresh();
  }

  componentDidUpdate = () => {
    const { logScrolled } = this.state;
    // Scroll to bottom of chat log
    if (this.logRef.current && !logScrolled) {
      this.logRef.current.scrollTop = this.logRef.current.scrollHeight;
      this.setState({
        logScrolled: true
      });
    }
  };

  onSelect = (data, type) => {
    console.log(data);
    this.setState({
      selected: data
    });
  };

  togleFilter = filter => async () => {
    const { history } = this.props;
    const token = cookie.get('jwt');
    const nextState = filter.state ? 'off' : 'on';
    const slug = filter.slug;
    try {
      const res = await axios.post(
        `${BACKEND_URL}/admin/filters/${slug}/${nextState}`,
        null,
        {
          headers: {
            Authorization: token
          }
        }
      );
      const updatedFilter = res.data;
      this.setState({
        filters: this.state.filters.map(f =>
          f.slug === updatedFilter.slug ? updatedFilter : f
        )
      });
    } catch (err) {
      console.error(err);
      if (err.response && err.response.status === 401) {
        cookie.remove('jwt');
        history.push('/login?next=/admin/shoutbox');
        return;
      }

      this.setState({
        loading: false,
        error: 'Error while togling filter, check console'
      });
    }
  };

  handleWordDelete = word => async () => {
    const { history } = this.props;
    const token = cookie.get('jwt');
    const id = word._id;
    try {
      // eslint-disable-next-line no-unused-vars
      const res = await axios.delete(
        `${BACKEND_URL}/admin/words/remove/${id}`,
        {
          headers: {
            Authorization: token
          }
        }
      );
      this.setState({
        words: this.state.words.filter(w => w._id !== id)
      });
    } catch (err) {
      console.error(err);
      if (err.response && err.response.status === 401) {
        cookie.remove('jwt');
        history.push('/login?next=/admin/shoutbox');
        return;
      }

      this.setState({
        loading: false,
        error: 'Error while deleting banned word, check console'
      });
    }
  };

  isSelected = (msg, other) => {
    return msg && other && msg._id === other._id;
  };

  refresh = () => {
    this.fetchMessages();
    this.fetchFilters();
    this.fetchWords();
  };

  wordFieldHandleChange = event => {
    this.setState({ wordField: event.target.value });
  };

  wordFieldHandleSubmit = async event => {
    event.preventDefault();
    const { history } = this.props;
    const token = cookie.get('jwt');
    const word = this.state.wordField;
    try {
      await axios.post(`${BACKEND_URL}/admin/words/${word}`, null, {
        headers: {
          Authorization: token
        }
      });
      this.fetchWords();
    } catch (err) {
      console.error(err);
      if (err.response && err.response.status === 401) {
        cookie.remove('jwt');
        history.push('/login?next=/admin/shoutbox');
        return;
      }
    }
    this.setState({ wordField: '' });
  };

  renderContent = () => {
    const {
      messages,
      filters,
      selected,
      loading,
      words,
      wordField
    } = this.state;
    const { history } = this.props;
    if (loading) {
      return <h2>Loading...</h2>;
    }

    let filterRows;
    if (filters.length === 0) {
      filterRows = [<div key="karhu">No filters availalbe.</div>];
    } else {
      filterRows = (
        <ul>
          {filters.map(filter => {
            const state = filter.state ? 'ON' : 'OFF';
            const nextState = filter.state ? 'OFF' : 'ON';

            return (
              <li key={filter.slug}>
                <strong>{filter.slug}</strong> {filter.description}, state:{' '}
                {state}&nbsp;
                <button onClick={this.togleFilter(filter)}>
                  Turn {nextState}!
                </button>
              </li>
            );
          })}
        </ul>
      );
    }

    let messageRows, count;
    if (messages.length === 0) {
      messageRows = <div>No shoutbox messages.</div>;
    } else {
      messageRows = messages.map((m, index) => (
        <Message
          selected={this.isSelected(m, selected)}
          onSelect={this.onSelect}
          key={index}
          data={m}
        />
      ));
      count = messageRows.length;
    }

    return (
      <Fragment>
        <h2>Shoutbox filters</h2>
        <Column>{filterRows}</Column>
        <h2>Banned words</h2>
        <Column>
          <BannedWordsPanel
            words={words}
            field={wordField}
            handleChange={this.wordFieldHandleChange}
            handleDelete={this.handleWordDelete}
            handleSubmit={this.wordFieldHandleSubmit}
          />
        </Column>
        <h2>Shoutbox messages</h2>
        {count && <Count>Total: {count} shoutbox messages(s)</Count>}
        <Column>
          <Log ref={this.logRef}>{messageRows}</Log>
        </Column>
        <Column hidden={!selected}>
          <MessagePanel
            refresh={this.refresh}
            history={history}
            data={selected}
          />
        </Column>
      </Fragment>
    );
  };

  render() {
    return (
      <Container>
        <AdminNavbar />
        {this.renderContent()}
      </Container>
    );
  }
}

export default AdminShoutboxPage;
