import React, { Component } from 'react';
import styled from 'styled-components';
import GoogleLogin from 'react-google-login';
import { checkJWTAuth } from '../../../utils';
import Cookie from 'universal-cookie';
import { parseSearch, environment } from '../../../utils';

const cookie = new Cookie();

const CLIENT_ID = environment.REACT_APP_OAUTH_CLIENT_ID;

const Container = styled.div`
  display: block;

  i,
  button {
    display: block;
  }
`;

const Login = styled(GoogleLogin)`
  padding: 1rem 1.5rem;
  margin: 1rem 0;
  font-size: 1rem;
  background-color: ${p => p.theme.color.pink100};
  color: ${p => p.theme.color.white100};
  outline: none;
  border: none;
  cursor: pointer;
`;

const Error = styled.i`
  color: ${p => p.theme.color.turquoise100};
  margin: 1rem 0;
`;

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
    }
    const jwt = cookie.get('jwt');
    if (jwt) {
      this.responseGoogle({
        tokenId: jwt
      });
    }
  }

  responseGoogle = async resp => {
    console.log(resp);
    if (resp.error) {
      console.log('Login error.')
      cookie.remove('jwt');
      this.setState({
        error: resp.error,
      });
      return;
    }

    const { location, history } = this.props;
    const { search } = location;
    const params = parseSearch(search);
    const next = params.next || '/admin';

    const id_token = resp.tokenId;
    const ok = await checkJWTAuth(id_token);
    if (ok) {
      cookie.set('jwt', id_token);
      history.push(next);
    } else {
      cookie.remove('jwt');
      console.log('Backend auth verification failed.');
      this.setState({
        error: 'Backend auth verification failed.',
      });
    }
  }

  render() {
    const { error } = this.state;
    return (
      <Container>
        <h2>Log in to Radiodiodi admin</h2>
        <i>Use your Radiodiodi.fi domain email.</i>
        <Login
          clientId={CLIENT_ID}
          buttonText="Log in"
          onSuccess={this.responseGoogle}
          onFailure={this.responseGoogle}
          hostedDomain="radiodiodi.fi"
          responseType="jwt"
          signedIn
        />
        {error && <Error>Error: {error}</Error>}
      </Container>
    );
  }
}

export default LoginPage;
