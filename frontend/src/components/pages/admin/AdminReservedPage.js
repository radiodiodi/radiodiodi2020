import React, { Component, Fragment } from 'react';
import axios from 'axios';
import ReservedName from '../../admin/ReservedName';
import ReservedNamePanel from '../../admin/ReservedNamePanel';
import Cookie from 'universal-cookie';
import AdminNavbar from '../../admin/AdminNavbar';
import { Count, Column, Container, Log } from '../../admin/Styles';
import { environment } from '../../../utils';

const cookie = new Cookie();

const BACKEND_URL = environment.REACT_APP_BACKEND_HTTP_URL;

class AdminReservedPage extends Component {
  state = {
    reservedNames: [],
    selected: null,
    loading: true,
    logScrolled: false
  };
  logRef = React.createRef();

  fetchReservedUsers = async () => {
    const { history } = this.props;
    const token = cookie.get('jwt');

    try {
      const resp = await axios.get(`${BACKEND_URL}/admin/users/reserved`, {
        headers: {
          Authorization: token
        }
      });
      const reservedNames = resp.data.reserved;
      this.setState({
        reservedNames,
        loading: false
      });
    } catch (err) {
      console.error(err);
      if (err.response && err.response.status === 401) {
        cookie.remove('jwt');
        history.push('/login?next=/admin/reserved_users');
        return;
      }

      this.setState({
        loading: false,
        error: 'Error, check console'
      });
    }
  };

  componentDidMount() {
    this.refresh();
  }

  componentDidUpdate = () => {
    const { logScrolled } = this.state;
    // Scroll to bottom of chat log
    if (this.logRef.current && !logScrolled) {
      this.logRef.current.scrollTop = this.logRef.current.scrollHeight;
      this.setState({
        logScrolled: true
      });
    }
  };

  onSelect = (data, type) => {
    console.log(data);
    this.setState({
      selected: data
    });
  };

  isSelected = (msg, other) => {
    return msg && other && msg._id === other._id;
  };

  refresh = () => {
    this.fetchReservedUsers();
  };

  renderContent = () => {
    const { reservedNames, selected, loading } = this.state;
    const { history } = this.props;

    if (loading) {
      return <h2>Loading...</h2>;
    }

    let reservedNameRows, count;
    if (reservedNames.length === 0) {
      reservedNameRows = <div>No reserved names.</div>;
    } else {
      reservedNameRows = reservedNames.map((m, index) => (
        <ReservedName
          selected={this.isSelected(m, selected)}
          onSelect={this.onSelect}
          key={index}
          data={m}
        />
      ));
      count = reservedNameRows.length;
    }

    return (
      <Fragment>
        {count && <Count>Total: {count} reserved name(s)</Count>}
        <Column>
          <Log ref={this.logRef}>{reservedNameRows}</Log>
        </Column>
        <Column hidden={!selected}>
          <ReservedNamePanel
            refresh={this.refresh}
            history={history}
            data={selected}
          />
        </Column>
      </Fragment>
    );
  };

  render() {
    return (
      <Container>
        <AdminNavbar />
        <h2>Reserved users</h2>
        {this.renderContent()}
      </Container>
    );
  }
}

export default AdminReservedPage;
