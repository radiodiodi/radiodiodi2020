import React, { Component } from 'react';
import axios from 'axios';
import Cookie from 'universal-cookie';
import AdminNavbar from '../../admin/AdminNavbar';
import Registration from '../../admin/Registration';
import RegistrationPanel from '../../admin/RegistrationPanel';
import { Count, Column, Container, Log } from '../../admin/Styles';
import { environment } from '../../../utils';

const cookie = new Cookie();

const BACKEND_URL = environment.REACT_APP_BACKEND_HTTP_URL;

class AdminRegistrationsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      registrations: [],
      loading: true,
      error: null,
      selected: null,
      logScrolled: false
    };
    this.logRef = React.createRef();
    this.fetchRegistrations();
  }

  fetchRegistrations = async () => {
    const { history } = this.props;
    const token = cookie.get('jwt');

    try {
      const resp = await axios.get(`${BACKEND_URL}/admin/registrations`, {
        headers: {
          Authorization: token
        }
      });
      const registrations = resp.data;
      this.setState({
        registrations,
        loading: false
      });
    } catch (err) {
      console.error(err);
      if (err.response && err.response.status === 401) {
        cookie.remove('jwt');
        history.push('/login?next=/admin/registrations');
        return;
      }

      this.setState({
        loading: false,
        error: 'Error, check console'
      });
    }
  };

  isSelected = (msg, other) => {
    return msg && other && msg._id === other._id;
  };

  onSelect = data => {
    console.log(data);
    this.setState({
      selected: data
    });
  };

  componentDidUpdate = () => {
    const { logScrolled } = this.state;
    // Scroll to bottom of chat log
    if (this.logRef.current && !logScrolled) {
      this.logRef.current.scrollTop = this.logRef.current.scrollHeight;
      this.setState({
        logScrolled: true
      });
    }
  };

  renderRegistrationRows = () => {
    const { registrations, loading, error, selected } = this.state;

    if (error) {
      return error;
    }

    if (loading) {
      return { rows: 'Loading...' };
    }

    if (registrations.length === 0) {
      return { rows: 'No registrations yet.' };
    }

    const rows = registrations.map((m, index) => (
      <Registration
        selected={this.isSelected(m, selected)}
        onSelect={this.onSelect}
        key={index}
        data={m}
      />
    ));

    return { rows, count: rows.length };
  };

  render() {
    const { selected } = this.state;
    const { history } = this.props;
    const { rows, count } = this.renderRegistrationRows();
    return (
      <Container>
        <AdminNavbar />
        <h2>Registrations</h2>
        {count && <Count>Total: {count} registration(s)</Count>}
        <Column>
          <Log ref={this.logRef}>{rows}</Log>
        </Column>
        <Column hidden={!selected}>
          <RegistrationPanel
            refresh={this.refresh}
            history={history}
            data={selected}
          />
        </Column>
      </Container>
    );
  }
}

export default AdminRegistrationsPage;
