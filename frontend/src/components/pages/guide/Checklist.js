import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Programme from './Programme';

const Container = styled.div`
  strong {
    color: ${p => p.theme.color.realOrange};
  }
`;

const Checklist = () => {
  return (
    <>
      <Container>
        <h3>Checklist</h3>
        <ol>
          <li>Keksi ohjelmaidea</li>
          <li>Kerää tekijäkaverit</li>
          <li>Suunnittele ohjelmasi! Hyvin suunniteltua ohjelmaa on mukava tehdä lähetyspäivänä

          </li>

          <li><Link to="/ilmo">ILMOITTAUDU!</Link></li>
          <li>Jos tarvitset apua tai jokin on epäselvää, ota yhteys Radiodiodin toimitukseen: <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi</a></li>
          <li>Halutessasi voit lähettää oma korkealaatuisen ohjelmantekijä kuvasi ohjelmakalenteria varten osoitteeseen <a href="mailto:kuvat@radiodiodi.fi">kuvat@radiodiodi.fi</a>. Kuvan tulee toimia kuvasuhteessa 1:1</li>
          <li>Ole paikalla vähintään 15 minuuttia ennen lähetyksesi alkua.</li>
          <li>Tee ohjelma ja pidä hauskaa</li>
          <li>Täytä ohjelmantekijäpalaute</li>
          {/*<li>Osallistu kaatoon!</li>*/}
        </ol>

        <h3>Ohjelman suunnittelun perusasiat</h3>
        <p><strong>Nimi.</strong> Ohjelman nimi on tärkein yksittäinen asia, jolla herätetään potentiaalisen kuulijan mielenkiinto. Erotu joukosta ja päräytä!</p>
        <p><strong>Ohjelman kuvaus.</strong> Suunnittele huolella, mikä saisi sinut kiinnostumaan ohjelmasi kuuntelusta?</p>
        <p><strong>Tavoite/tehtävä.</strong> Mitä haluan ohjelmallani saavuttaa tai viestiä? Viihdyttää? Opettaa? Kenties tarjota lisäarvoa kuulijalle?
      Nostaa esiin jotain mikä katoaa massaan? Jakaa unohdettuja mahdollisuuksia? Keskustella? </p>
        <p><strong>Teema/tyyppi.</strong> Mihin ohjelmasi keskittyy ja millainen se on sisällöltään?</p>
        {/* <p><strong>Aikataulu.</strong> Aamuyön ohjelma voi olla hyvinkin erilaista kuin keskipäivän ohjelma.</p> */}
        <p><strong>Kohdeyleisö.</strong> Ketä kuulijat ovat?</p>
        <p><strong>Ohjelmaelementit.</strong> Haastatteluja? Musiikkia? Live-puheluita? Erikoisvieraita?</p>
        <p><strong>Mainokset.</strong> Mainokset ovat osa ohjelmaa, ja niistä huolehtii tuottaja.</p>
      </Container>
      <Programme />
    </>
  );
}

export default Checklist;
