import React, { Fragment } from 'react';
import styled from 'styled-components';

const Container = styled.div`
`;

const Question = styled.i`
  &::before {
    content: 'Q: ';
  }

  display: block;
  margin: 1rem 0 0.5rem;
`;

const Answer = styled.div`
  margin: 1rem 0 3rem 1.4rem;
`;

const qas = [
  {
    question: 'Mikä on Radiodiodi?',
    answer: 'Radiodiodi on Otaniemestä ponnistava täysin opiskelijavetoisesti toteutettu wappuradio. Projektiin osallistuu vuosittain lähes sata vapaaehtoisia taustatyön muodossa ja ohjelmaakin tekee kahden lähetysviikon aikana parhaimmillaan yli 500 vapaaehtoista.'
  },
  {
    question: 'Missä Radiodiodi kuuluu?',
    answer: `Radiotaajuudella 105.8 MHz pääkaupunkiseudulla ja verkossa osoitteessa <a href="https://radiodiodi.fi">radiodiodi.fi.</a>`
  },
  {
    question: 'Voinko kuunnella Radiodiodia muuten kuin verkkosivujen soittimella?',
    answer: `Kyllä! Tästä löydät .m3u- soittolistatiedoston, josta löydät eri streamit: <a href="https://static.radiodiodi.fi/radiodiodi.m3u" download>radiodiodi.m3u</a>
    <p>Streamien osoitteet:</p>
    <ul>
      <li>Paras laatu, FLAC, häviöttömästi pakattu, ~ 1,3 mbit/s <a href="https://virta.radiodiodi.fi/flac">https://virta.radiodiodi.fi/flac</a></li>
      <li>"Normaali" laatu, mp3 256k bitrate <a href="https://virta.radiodiodi.fi/mp3">https://virta.radiodiodi.fi/mp3</a></li>
      <li>"Huono" laatu, aac 64k bitrate <a href="https://virta.radiodiodi.fi/aac">https://virta.radiodiodi.fi/aac</a></li>
    </ul>
    <p>Suosittelemme esimerkiksi <a href="https://www.foobar2000.org/">Foobar2000</a> tai <a href="https://www.videolan.org/">VLC</a> sovellusta, VLC:n löydät myös Google Playstä ja AppStoresta.`
  },
  {
    question: 'Milloin lähetys on käynnissä?',
    answer: 'Lähetys kuuluu ennen Wappua 14.-30.4.'
  },
  {
    question: 'Missä radion studio sijaitsee?',
    answer: 'Pop-Up studio tulee olemaan jossain päin Otaniemeä. Sijainti tarkentuu lähempänä lähetystä.'
  },
  {
    question: 'Kuka voi tulla tekemään ohjelmaa?',
    answer: 'Kuka tahansa, joka ajan kerkeää varaamaan. Radiodiodin toiminta on opiskelijoille täysin ilmaista! Aikataulukonflikteissa priorisoimme opiskelijoita.'
  },
  {
    question: 'Sopiiko ideani radiodiodiin ohjelmaksi?',
    answer: `Jos se ei riko lakia ja voisi mahdollisesti kiinnostaa jotakuta, kyllä!

    <p>Radiodiodissa on sisällöllisesti ollut ohjelmaa saatananpalvonnasta kristilliseen ohjelmaan ja musiikin analysoinnista tarinankerrontaan.
    Teknisesti ohjelma voi sisältää vaikkapa puhetta, nauhalta soitettua musiikkia, livemusiikkia, freestyleräppiä, DJ-settejä ja ääniefektejä.</p>

    <p>Huom: Jos ohjelma sisältää jotakin muuta kuin musiikin soittamista ja puhumista, ota yhteyttä etukäteen.</p>
  `},
  // , jotta toimitus voi valmistella studion erikoisempaa ohjelmaa varten. Autamme mielellämme typerienkin ideoidesi toteuttamisessa
  {
    question: 'Kuinka monta henkilöä lähetykseen mahtuu?',
    answer: 'Tuottajan lisäksi Radiodiodin studion lähetyspöydän ympärille mahtuu 4 henkilöä.'
  },
  {
    question: 'Mitä musiikkia voin soittaa?',
    answer: 'Kaikkea laillisesti hankittua musiikkia, kuten CD-levyt, vinyylit, kasetit ja itse tuotetut teokset. Myös mm. Suomen iTunesista tai Bandcampista ostetut kappaleet käyvät. Suoratoistopalvelut eivät ole hyväksyttävä lähde, joten esimerkiksi Spotify tai Youtube eivät ole käytettävissä. Jos haluat omat levysi <a href="/library"> meidän musiikkikantaan</a> helposti soitettavaksi, tuo ne studiolle kopioitavaksi vähintään 3 päivää ennen omaa ohjelmaasi. Toimitamme levysi takaisin ohjelmasi jälkeen. <p> Halutessasi voit myös toimittaa erillisen listan biiseistä, jotka haluaisit toimituksen hankkivan lähetystäsi varten Play Storesta tai iTunesista. Veloitamme ostetuista kappaleista hankinnasta koituvien kulujen mukaisesti.</p>'
  },
  {
    question: 'Mitä en saa tehdä radiossa?',
    answer: 'Älä soita yllämainittuja laittomia sisältöjä, äläkä ole idiootti.<br/><br/>Radiodiodi <strong>ei lähetä</strong> ohjelmaa, joka sisältää rasismia, seksismiä, ableismia, trans– tai homofobiaa. Radion toimituksella on oikeus keskeyttää ohjelma, mikäli väärinkäytöksiä ilmenee.'
  },
  {
    question: 'Mitä teknisiä mahdollisuuksia lähetyksessä on?',
    answer: `<p>Toimituksella on laajat mahdollisuudet toistaa ääntä eri laitteilla, tarpeesi mukaan. Muun muassa:</p>
    <ul>
      <li>Kaikkea <a href="/library">musiikkikannassa</a> olevaa musiikkia voi soittaa lähetyksen aikana</li>
      <li>DJ-mikseri CD-deckillä ja kahdella vinyylideckillä</li>
    </ul>

    Jos teet mitään muuta kuin pelkkää puheohjelmaa tai valmiista äänilähteistä soittamista, ota aina yhteyttä etukäteen. Ota myös rohkeasti yhteyttä, jos et ole jostain asiasta varma. Toimituksen saa kiinni osoitteesta <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi</a>.</p>`
  },
  // {
  //   question: 'Saako studiossa syödä/juoda? Entä alkoholi?',
  //   answer: 'Vältä syömistä studiotiloissa. Alkoholi on kielletty sillä lähetys tapahtuu koulun tiloista.'
  //   // answer: 'Kunhan laitteet pysyvät ehjänä ja kunnossa, mikään ei estä studiossa syömistä tai juomista.'
  // },
  {
    question: 'Miten mainosten soittaminen tapahtuu?',
    answer: 'Kaikissa päiväaikaan lähetettävissä ohjelmissa soitetaan mainoksia. Tuottaja huolehtii näiden soittamisesta käytännössä, mutta varaathan ohjelmastasi yhteensä n. 5 minuuttia tunnissa mainoksille ja jingleille.'
  }, {
    question: 'Mikä on jingle?',
    answer: 'Jingle on lyhyt audiopätkä, jossa kerrotaan mitä radioasemaa kuuntelet. Esimerkkinä "Kuuntelet Radio Novaa taajudella 106.2” Näitäkin soitetaan oman ohjelman ohessa tuottajan toimesta; yleensä mainosten yhteydessä.'
  }, {
    question: 'Voinko äänittää jinglen omalle ohjelmalleni?',
    answer: `Kyllä! Voit nauhoittaa jinglen itse ja lähettää sen meille etukäteen.
    Huomaathan, että tuotantoarvomme ovat korkealla: äänen täytyy olla ammattimaisesti nauhoitettua ja jälkituotettua.
    Ota yhteyttä <a href="mailto:studio@radiodiodi.fi">studio@radiodiodi.fi</a> jos tämä kiinnostaa.`
  }, {
    question: 'Saanko ohjelmani talteen jotenkin?',
    answer: 'Suosittelemme oman nauhoitteen tekemistä. Lakiteknisistä syistä nauhoituksien jakaminen jälkikäteen ei ole mahdollista.'
  },
   {
     question: 'Voinko nauhoittaa ohjelman etukäteen?',
     answer: 'Tämäkin on mahdollista. Ilmoita tästä jo ohjelmantekijäilmoittautumisen yhdeyssä. Voit myös ilmoittaa ohjelmatavan muutoksesta meille osoitteeseen <a href="mailto:studio@radiodiodi.fi">studio@radiodiodi.fi</a>.'
   },
   {
    question: 'Voinko tehdä etälähetyksen?',
    answer: 'Tämäkin on mahdollista. Ilmoita tästä jo ohjelmantekijäilmoittautumisen yhdeyssä. Voit myös ilmoittaa ohjelmatavan muutoksesta meille osoitteeseen <a href="mailto:studio@radiodiodi.fi">studio@radiodiodi.fi</a>.'
  },
  {
    question: 'Mikä on tuottaja?',
    answer: 'Tuottaja vastaa lähetyksen teknisestä puolesta, jotta tekijät saat keskittyä itse ohjelman tekemiseen. Hän huolehtii mm. mikrofonien äänenvoimakkuudesta sekä jinglejen, mainoksien ja musiikin soittamisesta. Hän on lähetyksessä paikalla ja voi myös osallistua ohjelmantekoon, tai tehdä ohjelmaa kokonaan itse. Kaikista näistä voi sopia tuottajan kanssa ennen lähetystä.'
  },
  {
    question: 'Minua voisi kiinnostaa tuottaminen. Mistä saan tietää siitä lisää?',
    answer: 'Tuottajakoulutuksista tulee lisää infoa myöhemmin.'
    // answer: 'Laita sähköpostia <a href="mailto:studio@radiodiodi.fi">studio@radiodiodi.fi</a> ja liity Radiodiodin Telegram-kanaville. Tuottajakoulutuksia järjestetään kiinnostuneille studion rakennusviikolla ja ensimmäisen lähetysviikon aikana.'
  },
  {
    question: 'Mitä jos minulla ei ole tuottajaa ohjelmalleni?',
    answer: 'Mainitse tästä ilmoittautumisen yhteydessä. Toimitus järjestää paikalle tuottajan. Jos unohdit mainita asiasta, ota yhteyttä <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi</a>.'
  },
  {
    question: 'Voinko tehdä jotain muuta lähetyksen eteen?',
    answer: `Ehdottomasti! Radiodiodin eteen tehdään ympäri vuotta monenlaista hommaa!
    Joka vuonna projektissa tarvitaan monialaista osaamista: äänitekniikkaa, yrityssuhteita, markkinointia, rakentamista, lähetystekniikkaa,
    IT:tä sekä vaikka mitä muuta. Ota yhteyttä <a href="mailto:rekry@radiodiodi.fi">rekry@radiodiodi.fi</a> tai tule kysymään asiasta,
    jos sinua kiinnostaa olla mukana vielä tänä tai ensi vuonna!`,
  }
];

const renderQuestionAndAnswer = (question, answer, key) => {
  return (
    <Fragment key={key}>
      <Question dangerouslySetInnerHTML={{ __html: question }} />
      <Answer dangerouslySetInnerHTML={{ __html: answer }} />
    </Fragment>
  );
}

const FAQ = () => {
  return (
    <Container>
      <h2>UKK</h2>
      <h3>Faktoja Radiodiodin ohjelmatuotannosta</h3>
      <ul>
        <li>Radiodiodin ohjelmasisältö noudattaa Suomen lakia ja hyviä tapoja.</li>
        <li>Radiodiodin päätoimittaja toimituksensa kanssa päättää viime kädessä kaikesta radio-ohjelmaan liittyvästä.</li>
        <li>Radiodiodi on mainosyhteistyöllä rahoitettu non-profit wappuradio, mikä tarkoittaa muun muassa sitä, että kaikkien ohjelmien aikana soitetaan mainoksia.</li>
        {/* <li>Radio-ohjelman tuottaa Radiodiodin tuottaja tai koulutettu ohjelmantekijä.</li> */}
        <li>Radiodiodissa ohjelman tekeminen on mukavaa ja antoisaa!</li>
        <li>Ohjelman tekemisestä tai radiossa puhumisesta ei tarvitse aiempaa kokemusta.</li>
      </ul>
      <br />
      {qas.map((qa, index) => renderQuestionAndAnswer(qa.question, qa.answer, index))}
    </Container>
  );
}

export default FAQ;
