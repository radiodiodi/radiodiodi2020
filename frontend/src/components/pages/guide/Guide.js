import React from 'react';
import styled from 'styled-components';
import { Route, Redirect, Switch, Link as ReactLink } from 'react-router-dom';
import FAQ from './FAQ';
import Checklist from './Checklist';

const Link = styled(ReactLink)`
  margin-right: 1rem;
`;

const Guide = ({ match }) => {
  return (
    <main>
      <h1>Ohjelmantekijän opas</h1>
      <nav>
        <Link to={`${match.url}/checklist`}>Checklist</Link>
        <Link to={`${match.url}/faq`}>UKK</Link>
      </nav>
      <Switch>
        <Route path={`${match.url}/checklist`} component={Checklist} />
        <Route path={`${match.url}/faq`} component={FAQ} />
        <Redirect to={`${match.url}/checklist`} />
      </Switch>
    </main>
  );
}

export default Guide;
