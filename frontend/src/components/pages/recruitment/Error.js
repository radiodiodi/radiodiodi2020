import React, { Component } from 'react';
import styled from 'styled-components';

const Container = styled.div`
  margin-bottom: 2rem;
`;

const Paragraph = styled.div`
  max-width: 700px;
  flex-grow: 1;

  hr {
    width: 50%;
    text-align: left;
  }
`;

const Title = styled.h3`
  color: ${p => p.theme.color.turquoise100};
  margin: 0;
  height: 4rem;
`;

class RecruitmentError extends Component {
  render() {
    return (
      <Container>
        <Paragraph>
          <Title>Lomakkeen lähetys epäonnistui :(</Title>
          <Paragraph>
            Nyt kosahti! Jos lomakkeen uudelleentäyttäminen ei auta, otathan yhteyttä toimitukseen
            osoitteessa <a href="mailto:rekry@radiodiodi.fi">rekry@radiodiodi.fi</a>. Pahoittelut häiriöstä!
          </Paragraph>
        </Paragraph>
      </Container>
    );
  }
}

export default RecruitmentError;
