import React, { Component } from 'react';
import { withRouter } from "react-router";
import axios from 'axios';
import {
  Form as InformedForm,
  Text as InformedText,
  TextArea as InformedTextArea,
} from 'informed';
import styled from 'styled-components';
import { environment } from '../../../utils';

const BACKEND_URL = environment.REACT_APP_BACKEND_HTTP_URL;

const Form = styled(InformedForm)`
  margin: 3rem 0;

  input,
  text,
  textarea {
    font-family: inherit;
    font-size: 1rem;
    border: 3px inset ${p => p.theme.color.realOrange};
  }

  p {
    max-width: 70ch;
  }

  fieldset {
    border: none;
    border-top: 0.2rem solid ${p => p.theme.color.realOrange};
    margin-top: 2rem;
  }

  legend {
    font-size: 1.2rem;
    padding: 0 0.5em;
  }

  strong {
    color: #E3398B;
  }

`;

const Fieldset = styled.fieldset`
  @media screen and (min-width: 800px) {
    label {
      display: flex;
      justify-content: space-between;
    }
  }
`

const StyledText = styled(InformedText)`
  padding: 0.5rem;
  max-width: 500px;
  width: 100%;
`;

const StyledTextArea = styled(InformedTextArea)`
  padding: 0.5rem;
  max-width: 500px;
  width: 100%;
`;

const Text = (props) => (
  <StyledText validate={notEmptyString} {...props} />
);

const TextArea = (props) => (
  <StyledTextArea validate={!props.optional && notEmptyString} {...props} />
);

const Label = styled.label`
  display: block;
  margin: 1em 0;

  input:not([type="checkbox"]):not([type="radio"]),
  textarea {
    display: block;
  }

  input[type="radio"],
  input[type="checkbox"] {
    margin: 0 1rem;
  }
`;

const Submit = styled.button`
  display: block;
  width: 7em;
  padding: 1em 1.5em;
  margin: 2em auto;
  background-color: ${p => p.theme.color.realOrange};
  font-size: 1.1rem;
  color: ${p => p.theme.color.white100};

  border: none;
  outline: none;
  cursor: pointer;

  &:hover,
  &:focus {
    background-color: ${p => p.theme.color.darkOrange};
  }
`;

const Info = styled.p`
  margin-left: 2em;
  /* color: ${p => p.theme.color.realOrange}; */
  color: #bbb;
`

const Error = styled.div`
  color: red;
`;

const notEmptyString = value => !value ? 'Kenttää ei voi jättää tyhjäksi' : undefined;

class RecruitmentForm extends Component {
  setFormApi = (formApi) => {
    this.formApi = formApi;
  }

  handleSubmit = async () => {
    const { history } = this.props;
    const state = this.formApi.getState();
    console.log(state);

    try {
      const resp = await axios.post(`${BACKEND_URL}/api/recruitment`, state.values);
      console.log(resp);
      const email = state.values.email;
      history.push(`/rekry/success?email=${email}`);
    } catch (err) {
      console.error(err);
      history.push('/rekry/error');
    }
  }

  getErrors = () => this.formApi
    ? this.formApi.getState().errors || {}
    : {};

  getValues = () => this.formApi
    ? this.formApi.getState().values || {}
    : {};

  renderErrors = () => {
    const errors = this.getErrors();
    const hasErrors = !!Object.keys(errors).length;
    if (!hasErrors) return null;

    return (
      <Error>
        Lomakkeessa on virheitä. Tarkista vielä kenttien tiedot!
      </Error>
    )
  }

  rerender = () => {
    // force rerender
    this.setState({});
  }

  handleSubmitFailure = (data) => {
    this.rerender();
    return true;
  }

  render() {
    const errors = this.getErrors();

    return (
      <Form getApi={this.setFormApi}
        onSubmit={this.handleSubmit}
        onSubmitFailure={this.handleSubmitFailure}
      >
        <Fieldset>
          <Label>
            Nimi
            {errors.name && <Error>{errors.name}</Error>}
            <Text
              field="name"
              placeholder="Ripe Radioaalto" />
          </Label>
          <Label>
            Sähköpostiosoite
            {errors.email && <Error>{errors.email}</Error>}
            <Text
              field="email"
              type="email"
              placeholder="email@example.com"
            />
          </Label>
          <Label>
            Viesti
            {errors.message && <Error>{errors.message}</Error>}
            <TextArea
              field="message"
              placeholder="Kerro itsestäsi lyhyesti. Mikä innostaa radiotoiminnassa? Missä rooleissa olisit parhaimmillasi?"
              rows="4"
            />
          </Label>
        </Fieldset>
        <fieldset>
          {this.renderErrors()}
          <br />
          <Info style={{textAlign: "center", margin: "auto"}}>Lähettämällä lomakkeen hyväksyt, että annettuja tietoja säilytetään
          vuoden ajan ja poistetaan toimittajavalintojen jälkeen.
          </Info>
          <Submit type="submit">Lähetä</Submit>
        </fieldset>
      </Form>
    );
  }
}

export default withRouter(RecruitmentForm);
