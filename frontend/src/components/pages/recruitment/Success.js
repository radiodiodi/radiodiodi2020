import React, { Component } from 'react';
import styled from 'styled-components';

import { parseSearch } from '../../../utils';

const Container = styled.div`
  margin-bottom: 2rem;
`;

const Paragraph = styled.div`
  max-width: 700px;
  margin-bottom: 1rem;
  flex-grow: 1;

  hr {
    width: 50%;
    text-align: left;
  }
`;

const Title = styled.h3`
  margin: 0;
  height: 4rem;
`;

class RecruitmentSuccess extends Component {
  render() {
    const { location } = this.props;
    const { search } = location;
    const queryParams = parseSearch(search);
    const { email } = queryParams;

    return (
      <Container>
        <Paragraph>
          <Title>Lomakkeen lähetys onnistui!</Title>
          <Paragraph>
            Kiitämme mielenkiinnostasi Radiodiodin toimintaa kohtaan! 
            Olemme yhteydessä, kun ryhdymme valitsemaan uutta toimitusta.
          </Paragraph>
          { email && (
          <Paragraph>
            Vahvistus lomakkeen lähetyksestä on lähetetty sähköpostiosoitteeseen { email }.
          </Paragraph>
          )}
        </Paragraph>
      </Container>
    );
  }
}

export default RecruitmentSuccess;
