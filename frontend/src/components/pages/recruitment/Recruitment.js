import React from 'react';
import styled from 'styled-components';

// import RecruitmentForm from './RecruitmentForm';

const Main = styled.main`
  p {
    max-width: 700px;
  }
`;

const Recruitment = () => {
  return (
    <Main>
      <h1>Hae mukaan toimittamaan Radiodiodia!</h1>
      <p>
        Radiodiodia toimittaa noin 8-10 Aalto-yliopiston opiskelijaa, jotka valitaan vuodeksi kerrallaan.
        Aikaisempi vapaaehtoiskokemus ei ole vaatimuksena hakemiselle, vaan tärkeintä on innokkuus ja kiinnostus radiotoimintaa kohtaan!
      </p>
      <p>
        Kevään radio järjestetään yleensä kahtena Wappua edeltävänä viikkona ja lähetyskausi huipentuu Wappuaattoon.
        Toimituksella on kuitenkin vapaat kädet tehdä vuodesta omannäköisensä, ja esimerkiksi Teekkarius 150 -juhlavuoden kunniaksi myös syksyllä saatiin kuulla radiolähetyksiä!
      </p>
      <p>
        Jos sinulla on kysymyksiä Radiodiodiin tai hakemiseen liittyen, ota yhteyttä toimitukseen <a href="mailto:toimitus@radiodiodi.fi">toimitus@radiodiodi.fi</a>{' '}
        tai tulla nykäisemään toimittajaa hihasta!
        {/* Voit myös halutessasi jättää vapaamuotoisen hakemuksen osoitteeseen <a href="mailto:rekry@radiodiodi.fi">rekry@radiodiodi.fi</a>. */ }
      </p>
      
      <h3>Haku vuoden 2024 toimitukseen on sulkeutunut!</h3>
      {/* <RecruitmentForm /> */}
    </Main>
  );
}

export default Recruitment;
