import React from 'react';
import styled from 'styled-components';
import { environment } from '../../../utils';

const Paragraph = styled.p`
  text-align: left;
  max-width: 70ch;
`;

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: row;

  @media screen and (max-width: 1000px) {
    flex-direction: column;
  }

  justify-content: space-between;
`;

const Column = styled.span`
  margin-bottom: 1rem;
`;

const Title = styled.h2`
  text-align: left;
`;

const AudioPlayerContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: 0 -0.5rem;
`;

const AudioPlayer = styled.span`
  display: flex;
  flex-direction: column;
  margin: 0 0.5rem 1rem;

  @supports (-moz-appearance: none) {
    width: 30%; /* fixes firefox width bug */
  }

  @media screen and (max-width: 700px) {
    width: 100%;
    flex-direction: row;
    justify-content: space-between;
  }

  @media screen and (max-width: 500px) {
    width: 100%;
    flex-direction: column;
    align-items: center;
  }
`;

const AudioPlayerLabel = styled.label`
  margin-bottom: 0.5rem;

  @media screen and (max-width: 700px) {
    margin-bottom: 0;
    align-self: center;
  }

  @media screen and (max-width: 500px) {
    margin-bottom: 0.5rem;
  }
`;

const Companies = () => {
  return (
    <>
      <Paragraph>
        Ota yhteyttä yrityssuhdevastaavaamme:{' '}
        <a href="mailto:yrityssuhteet@radiodiodi.fi">
          yrityssuhteet@radiodiodi.fi
        </a>
      </Paragraph>

      <Title>Mitä meillä on tarjota yrityksille?</Title>
      <Paragraph>
        Jotta Suomen nopein kuulotelevisio voitaisiin aistia, tarvitaan ensin
        rahaa ja papereiden pyörittelyä.
      </Paragraph>
      <Paragraph>
        Radiodiodi tarjoaa yhteistyökumppaneilleen mahdollisuuden tavoittaa
        koko pääkaupunkiseudun opiskelijat ja vastavalmistuneet
        radioaalloilla, sekä koko Suomi netin välityksellä. Pop-up-studiomme
        sijaitsee Otaniemessä, Aalto-yliopiston kymmenien tuhansien
        opiskelijoiden ja työntekijöiden keskellä. Näymme
        opiskelijatapahtumissa ympäri vuoden ja tarjoamme pitkäaikaisia
        yhteistyökumppanuuksia.{' '}
      </Paragraph>
      <Paragraph>
        Yhteistyön laajuudesta riippuen, voimme tarjota mm. seuraavaa:
      </Paragraph>

      <ColumnContainer>
        <Column>
          <h4>
            Ainutlaatuisia opiskelijoihin vetoavia wappuradiomainoksia ja
            yhteisohjelmia
          </h4>
          <ul>
            <li>Tarjoamme radiomainoksianne lähetyksiimme</li>
            <li>
              Tuotamme mainosspotit huomioiden sekä teidän toiveenne, että
              yleisömme odotukset
            </li>
            <li>
              Yhteisohjelma on 1 - 2 tunnin täysin vapaamuotoinen ohjelma,
              jonka tuottamisessa voimme avustaa
            </li>
          </ul>
        </Column>
        <Column>
          <h4>Näkyvyyttä ja tapahtumia</h4>
          <ul>
            <li>Logopaikka nettisivuille, bannereihin, kuvaustaustoihin ja mainoksiin</li>
            <li>Räätälöityjä somepostauksia</li>
            <li>Näkyvyyttä keskellä Otaniemen wappua ja/tai orientaatioviikkoa lähetyskonteillamme</li>
            <li>
              Myös ohjelmantekijöiden erinäisiin tapahtumiin on mahdollista
              päästä mukaan
            </li>
          </ul>
        </Column>
      </ColumnContainer>

      <Paragraph>Kiinnostuitko? Ota yhteyttä!</Paragraph>
      <Paragraph>
        <a href="mailto:yrityssuhteet@radiodiodi.fi">
          yrityssuhteet@radiodiodi.fi
        </a>
      </Paragraph>
      <Title>Aiempien vuosien mainoksia</Title>

      <AudioPlayerContainer>
        <AudioPlayer>
          <AudioPlayerLabel>
            Vincit &ndash; ATK-ohjelmoitsija
          </AudioPlayerLabel>
          <audio controls>
            <source
              src={`${environment.REACT_APP_STATIC_URL}/audio/vincit1.wav`}
              type="audio/wav"
            />
          </audio>
        </AudioPlayer>

        <AudioPlayer>
          <AudioPlayerLabel>SRV &ndash; Diego &amp; Alberto</AudioPlayerLabel>
          <audio controls>
            <source
              src={`${environment.REACT_APP_STATIC_URL}/audio/srv1.wav`}
              type="audio/wav"
            />
          </audio>
        </AudioPlayer>

        <AudioPlayer>
          <AudioPlayerLabel>Futurice &ndash; Chilicorn.org</AudioPlayerLabel>
          <audio controls>
            <source
              src={`${environment.REACT_APP_STATIC_URL}/audio/futurice-spiceprogram-eng.wav`}
              type="audio/wav"
            />
          </audio>
        </AudioPlayer>
      </AudioPlayerContainer>
    </>
  );
}

export default Companies;
