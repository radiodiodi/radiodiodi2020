import React from 'react';
import Cookie from 'universal-cookie';
import styled from 'styled-components';
import { withRouter } from 'react-router';

const cookie = new Cookie();

const Button = styled.button`
  outline: none;
  border: none;
  padding: 0.5rem 1rem;
  color: ${p => p.theme.color.white100};
  background-color: ${p => p.theme.color.realOrange};
  font-size: 16px;
  cursor: pointer;
`;

const logout = history => () => {
  cookie.remove('jwt');
  history.push('/login');
};

const LogoutButton = ({ history }) => {
  return (
    <Button onClick={logout(history)}>Log out</Button>
  )
};

export default withRouter(LogoutButton);
