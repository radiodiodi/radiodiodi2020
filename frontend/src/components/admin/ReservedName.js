import React, { Component } from 'react';
import { LogRow } from './Styles';

class ReservedName extends Component {
  render() {
    const { data, onSelect } = this.props;
    return (
      <LogRow onClick={() => onSelect(data, 'reserved')}>
        { data.name }, IP: { data.ip }
      </LogRow>
    );
  }
}

export default ReservedName;