import React, { Component } from 'react';
import { LogRow, LogRowContrast } from './Styles';

class Registration extends Component {
  render() {
    const { data, onSelect } = this.props;
    return (
      <LogRow onClick={() => onSelect(data)}>
        {data.timestamp}, <LogRowContrast>{data.name}</LogRowContrast>
      </LogRow>
    );
  }
}

export default Registration;
