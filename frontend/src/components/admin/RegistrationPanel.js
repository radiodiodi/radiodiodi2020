import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  padding: 0.5rem;
  margin-bottom: 2rem;
  background-color: ${p => p.theme.color.white10};
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  @media screen and (max-width: 600px) {
    flex-direction: column;
    align-items: flex-start;
    margin-bottom: 0.5rem;

    .propositions {
      width: 100%;
    }
  }

  justify-content: space-between;
  padding: 0.2rem 0.5rem;
  border-bottom: 1px dotted ${p => p.theme.color.white100};

  div:last-of-type {
    text-align: right;
  }
`;

const renderPropositions = (propositions) => {
  if (!propositions) return null;
  return propositions.map((p, index) => (
    <div className="proposition" key={index}>
      {p.date}: {p.startTime}&ndash;{p.endTime}
    </div>
  ))
}

const Panel = ({ data }) => {
  if (!data) return null;
  console.log(data)
  return (
    <Container>
      <Row>
        <div>ID</div>
        <div>{ data._id }</div>
      </Row>
      <Row>
        <div>Registered at</div>
        <div>{ data.timestamp }</div>
      </Row>
      <Row>
        <div>Accept</div>
        <div>{ data.accept }</div>
      </Row>
      <Row>
        <div>Name</div>
        <div>{ data.name }</div>
      </Row>
      <Row>
        <div>Description</div>
        <div>{ data.description }</div>
      </Row>
      <Row>
        <div>Team</div>
        <div>{ data.team }</div>
      </Row>
      <Row>
        <div>Responsible</div>
        <div>{ data.responsible }</div>
      </Row>
      <Row>
        <div>Email</div>
        <div>{ data.email }</div>
      </Row>
      <Row>
        <div>Genre/type</div>
        <div>{ data.genre }</div>
      </Row>
      <Row>
        <div>Participants</div>
        <div>{ data.participants }</div>
      </Row>
      <Row>
        <div>Duration</div>
        <div>{ data.duration }</div>
      </Row>
      <Row>
        <div>Info</div>
        <div>{ data.info }</div>
      </Row>
      <Row>
        <div>Producer</div>
        <div>{ data.producer }</div>
      </Row>
      <Row>
        <div>Photoshoot date</div>
        <div>{ data.photoshoot }</div>
      </Row>
      <Row>
        <div>Time propositions</div>
        <div className="propositions">{ renderPropositions(data.propositions) }</div>
      </Row>
    </Container>
  )
}

export default Panel;
