import React, { Component } from 'react';
import styled from 'styled-components';
import { LogRow, LogRowContrast } from './Styles';

const Text = styled.span`
  text-overflow: ellipsis;
  overflow: hidden;
  min-width: 0;
`;

class Message extends Component {
  select = () => {
    const { data, onSelect } = this.props;
    onSelect(data, 'message');
  }

  render() {
    const { data, selected } = this.props;
    return (
      <LogRow selected={selected} onClick={this.select} >
        <span>{ data.timestamp } </span>
        <LogRowContrast>{ data.name }: </LogRowContrast>
        <Text>{ data.text }</Text>
      </LogRow>
    )
  }
}

export default Message;