import React, { useRef, useEffect, useCallback, useContext } from 'react';
import styled from 'styled-components';
import Cookie from 'universal-cookie';
import dateFormat from 'dateformat';
import { SocketContext } from './SocketProvider';
import { ChatContext } from './ChatProvider';

import verified from '../../svg/check-mark.svg';

const MAX_MESSAGES = 100;
const MESSAGE_MAX_LENGTH = 500;
const USERNAME_MAX_LENGTH = 16;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 1rem;
  max-height: calc(100vh - 2rem);
  background-color: ${p => p.theme.color.white10};
`;

const Log = styled.div`
  width: 100%;
  flex: 1;
  overflow: auto;
  font-size: 0.8rem;
  line-height: 1.1rem;

  scrollbar-color: #aaa #0000;
  scrollbar-width: thin;

  &::-webkit-scrollbar {
    width: 0.5rem;
  }

  &::-webkit-scrollbar-track {
    opacity: 0;
  }

  &::-webkit-scrollbar-thumb {
    background: #888;
  }

  &::-webkit-scrollbar-thumb:hover {
    background: #aaa;
  }
`;

const Row = styled.div`
  padding: 0.3rem;
  font-size: 1rem;
  ${p => p.error && `color: ${p => p.theme.color.turquoise100}`};
  display: flex;
  align-items: flex-start;
`;

const InputContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

const SendButton = styled.a`
  padding: 0.5rem;
  margin: 0.5rem;
  cursor: pointer;
  background-color: ${p => p.theme.color.white100};
  color: ${p => p.theme.color.grey100};

  &:hover {
    text-decoration: none;
    background-color: ${p => p.theme.color.darkOrange};
    color: ${p => p.theme.color.white100};
  }
`;

const Username = styled.input`
  padding: 0.5rem;
  margin: 0.5rem;
  flex: 1;
`;

const InputRow = styled.div`
  flex: 4;
  display: flex;
`;

const Prompt = styled.input`
  flex: 1;
  padding: 0.5rem;
  margin: 0.5rem;
`;

const Verified = styled.img`
  width: 16px;
  margin: 0 0.3rem 0 0;
`;

const Timestamp = styled.div`
  white-space: nowrap;
  margin-right: 0.3rem;
  display: flex;
  justify-content: space-between;
  font-size: 0.8rem;
  font-weight: bold;
  opacity: 0.8;
`;

const RowText = styled.span`
  word-break: break-word;
`;

const RowUser = styled.span`
  white-space: pre;
  font-weight: bold;
  color: ${p => p.theme.color.realOrange};

  &::after {
    content: ': ';
    color: ${p => p.theme.color.white100};
  }
`;

const Block = styled.span`
  display: block;
  word-break: break-all;
`;

const Shoutbox = () => {
  const connection = useContext(SocketContext);
  const logContext = useContext(ChatContext);
  const cookieRef = useRef(null);
  const chatLogRef = useRef(null);
  const usernameRef = useRef(null);
  const msgRef = useRef(null);

  const showError = useCallback((evt) => {
    logContext.setLog([
      {
        error: true,
        name: 'SERVER',
        text: 'Huutislaatikko ei saa yhteyttä! :(',
        timestamp: new Date()
      }
    ]);
  }, [logContext]);

  const handleData = useCallback((evt) => {
    const data = JSON.parse(evt.data);

    // Hack to get the current state of the log with the setter function
    let currentLog;
    logContext.setLog((current) => {
      currentLog = current;
      return current;
    });

    if (data.initial) {
      logContext.setLog(data.initial);
    } else if (data.message) {
      const newLog = currentLog.concat([data.message]);
      if (newLog.length > MAX_MESSAGES) {
        newLog.splice(0, 1);
      }
      logContext.setLog(newLog);
    } else if (data.erase) {
      const id = data.erase;
      const filtered = currentLog.filter(m => m._id !== id);
      logContext.setLog(filtered)
    }
  }, [logContext]);

  const sendMessage = () => {
    const text = msgRef.current.value,
      username = usernameRef.current.value;

    if (!username || username.length === 0) {
      alert('A username is required!');
      return;
    }

    if (!text || text.length === 0) {
      alert('A message is required!');
      return;
    }

    if (text.length > MESSAGE_MAX_LENGTH) {
      alert(`Message too long! Max: ${MESSAGE_MAX_LENGTH} characters.`);
      return;
    }

    if (username.length > USERNAME_MAX_LENGTH) {
      alert(`Username too long! Max: ${USERNAME_MAX_LENGTH} characters.`);
      return;
    }

    console.log(`Send message: ${text}, username: ${username}`);
    connection.send(
      JSON.stringify({
        name: username,
        text
      })
    );

    msgRef.current.value = '';
  };

  const setUsername = () => {
    const username = usernameRef.current.value
    cookieRef.current.set('username', username);
  }

  const renderRows = (rows) => {
    return rows.map((row, index) => (
      <Row error={row.error} key={index}>
        <Block>
          <Timestamp>
            {dateFormat(new Date(Date.parse(row.timestamp)), 'dd.mm HH:MM')}
          </Timestamp>
        </Block>
        <Block>
          {row.reserved && <Verified src={verified} />}
          <RowUser>{row.name}</RowUser>
          <RowText>{row.text}</RowText>
        </Block>
      </Row>
    ));
  };

  useEffect(() => {
    connection.addEventListener('message', handleData);
    connection.addEventListener('error', showError);

    return () => {
      connection.removeEventListener('message', handleData);
      connection.removeEventListener('error', showError);
    };
  }, [connection, handleData, showError]);

  useEffect(() => {
    cookieRef.current = new Cookie();
    const username = cookieRef.current.get('username') || '';
    usernameRef.current.value = username;
  }, [])

  useEffect(() => {
    chatLogRef.current.scrollTop = chatLogRef.current.scrollHeight;
  });

  const onPromptKeyPress = (event) => {
    const ENTER_KEY = 13;

    if ([event.keyCode, event.which].includes(ENTER_KEY)) {
      sendMessage();
    }
  };

  return (
    <Container>
      <Log ref={chatLogRef}>{renderRows(logContext.log)}</Log>

      <InputContainer>
        <Username
          placeholder="Username"
          ref={usernameRef}
          maxLength={USERNAME_MAX_LENGTH}
          onChange={setUsername}
          />
        <InputRow>
          <Prompt
            placeholder="Huutista..."
            onKeyPress={onPromptKeyPress}
            ref={msgRef}
            maxLength={MESSAGE_MAX_LENGTH}
            />
          <SendButton onClick={sendMessage}>
            &nbsp;{">"}&nbsp;
          </SendButton>
        </InputRow>
      </InputContainer>
    </Container>
  );
}

export default Shoutbox;
