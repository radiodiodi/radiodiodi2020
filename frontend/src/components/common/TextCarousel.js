import Carousel from 'nuka-carousel';
import React from 'react';
import styled from 'styled-components';

const CarouselWrapper = styled.div`
  margin-right: 2rem;
  margin-bottom: 0;

  strong {
    color: ${p => p.theme.color.realOrange};
  }
`;

export default class extends React.Component {
  render() {
    return (
      <CarouselWrapper>
        <Carousel withoutControls autoplay autoplayInterval={5000} wrapAround pauseOnHover width="100%" heightMode="max">
          <div>Radiodiodi palkittiin Hugo-gaalassa 2018 Aallon&nbsp;
            <a href="https://www.ayy.fi/fi/hugo-gaala-palkitsi-aalto-yhteison-pioneereja"><strong>parhaana yhteisönrakentajana</strong></a>.
          </div>
          <div>Vuonna 2019 tuotettiin <strong>163</strong>&nbsp;radio-ohjelmaa.</div>
          <div>
            Vuonna 2019 Radio lauloi yli kaksi wappua edeltävää viikkoa, eli yhteensä&nbsp;<strong>352</strong>&nbsp;tuntia,&nbsp;
            <strong>21&nbsp;120</strong>&nbsp;minuuttia tai&nbsp;<strong>1 267 200</strong>&nbsp;sekuntia.
          </div>
        </Carousel>
      </CarouselWrapper>
    );
  }
}
