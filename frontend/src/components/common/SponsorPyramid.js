import React from 'react';
import styled from 'styled-components';
import { environment } from '../../utils';

const tek = `${environment.REACT_APP_STATIC_URL}/img/2022/sponsors/tek.png`;
const siili = `${environment.REACT_APP_STATIC_URL}/img/2022/sponsors/Siili_white.png`;
const knowit = `${environment.REACT_APP_STATIC_URL}/img/2022/sponsors/knowit.png`;
const granlund = `${environment.REACT_APP_STATIC_URL}/img/2023/sponsors/granlund_white.png`;
const nitor = `${environment.REACT_APP_STATIC_URL}/img/2023/sponsors/nitor_white.svg`;
const fonum = `${environment.REACT_APP_STATIC_URL}/img/2023/sponsors/fonum_white.png`;


const SponsorContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
  align-content: flex-start;
  align-items: center;
  margin: 1rem 0 0 -2rem;;

  & > * {
    margin: 0 0 2rem 2rem;
    height: 80px;
    width: 160px;

    ${props => props.medium && `
      height: 150px;
      width: 300px;
    `}

    ${props => props.large && `
      height: 250px;
      width: 500px;
    `}

    ${props => props.xl && `
      height: 400px;
      width: 750px;
    `}

    img {
      width: 100%;
      height: 100%;
      object-fit: contain;
    }
  }
`;


const Sponsor = ({ src, alt }) => <div><img src={src} alt={alt} loading="lazy" /></div>;

export const SponsorPyramid = () => {
  return (
    <div>
      <SponsorContainer large>
        <Sponsor src={siili} alt="Siili" />
      </SponsorContainer>
      <SponsorContainer medium>
        <Sponsor src={tek} alt="TEK" />
        <Sponsor src={knowit} alt="Knowit" />
        <Sponsor src={nitor} alt="Nitor" />
        <Sponsor src={fonum} alt="Fonum" />
        <Sponsor src={granlund} alt="Granlund" />
      </SponsorContainer>
    </div>
  )
}

export default SponsorPyramid;
