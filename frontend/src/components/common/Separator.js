import styled from 'styled-components';

const Separator = styled.div`
  margin: 1rem 0 2rem;
  width: ${p => p.size || '100%'};
  height: 10px;

  &:not(.single) {
    border-top: 4px solid ${p => p.theme.color.orange100};
  }

  &:is(.center) {
    margin: 1rem auto 2rem;
  }

  border-bottom: 2px solid ${p => p.theme.color.turquoise100};
`;

export default Separator;
