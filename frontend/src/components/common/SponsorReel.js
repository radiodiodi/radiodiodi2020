import React, { Component } from 'react';
import styled from 'styled-components';
// import shuffle from 'shuffle-array';
import FadeImage from './FadeImage';
import { environment } from '../../utils';


const futurice = `${environment.REACT_APP_STATIC_URL}/img/2020/futurice.svg`;
const siili = `${environment.REACT_APP_STATIC_URL}/img/2020/siili.png`;
// const kmarket =   `${environment.REACT_APP_STATIC_URL}/img/2020/kmarket.png`;
// const cybercom =  `${environment.REACT_APP_STATIC_URL}/img/2020/cyber_full.png`;
// const luja =      `${environment.REACT_APP_STATIC_URL}/img/2020/luja.png`;
// const tek =       `${environment.REACT_APP_STATIC_URL}/img/2020/TEK_negative.png`;
//const tter = `${environment.REACT_APP_STATIC_URL}/img/2020/TTER.png`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const ReelImage = styled(FadeImage)`
  height: 120px;
  margin: 2rem auto;
  object-fit: contain;
  max-width: min(400px, 95%);
  min-width: 200px;
`;

const Title = styled.small`
  text-align: center;
  margin-bottom: 0.5rem;
`;

class SponsorReel extends Component {
  constructor() {
    super();
    this.state = {
      current: 0,
      counter: 0,
      images: [
        siili,
        siili,
        futurice,
        // kmarket,
        // cybercom,
        // luja,
        // tek,
        //tter,
      ],
    };
    this.updateCurrent = this.updateCurrent.bind(this);
  }

  componentDidMount() {
    const intervalHandle = window.setInterval(this.updateCurrent, this.props.interval);
    this.setState({
      intervalHandle,
    });
  }

  componentWillUnmount() {
    const { intervalHandle } = this.state;
    window.clearInterval(intervalHandle);
  }

  async updateCurrent() {
    const { counter, images } = this.state;
    this.setState({
      counter: counter + 1,
      current: (counter + 1) % images.length,
    });
  }

  render() {
    const { images, current } = this.state;
    const image = images[current];
    return (
      <Container>
        <Title><strong>Yhteistyössä</strong></Title>
          <ReelImage
            src={image}
          />
      </Container>
    );
  }
}

export default SponsorReel;
