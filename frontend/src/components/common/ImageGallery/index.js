import React from 'react';
import styled from 'styled-components';

import Person from './Person';
import { environment } from '../../../utils';

const Gallery = styled.section`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
`;

const IMG_URL = `${environment.REACT_APP_STATIC_URL}/img/2022/toimitus/`;

const people = [
  {
    name: 'Olli',
    role: 'Päätoimittaja',
    img: `${IMG_URL}olli.jpg`,
    mail: 'titta@radiodiodi.fi'
  },
  {
   name: 'Emma',
   role: 'Markkinointivastaava',
   img: `${IMG_URL}emma.jpg`,
  mail: '@radiodiodi.fi'
  },
  {
    name: 'Vili',
    role: 'Oltermanni',
    img: `${IMG_URL}vili.jpg`,
    mail: 'ville.eronen@radiodiodi.fi'
  },
  {
    name: 'Veikka',
    role: 'Studiopäällikkö',
    img: `${IMG_URL}veikka.jpg`,
    mail: 'ville.jaaskelainen@radiodiodi.fi'
  },
{
     name: 'Rasmus',
     role: 'Yrityssuhdevastaava',
     img: `${IMG_URL}rasmus.jpg`,
     mail: '@radiodiodi.fi'
   },
  {
    name: 'Maria',
    role: 'Graafikko',
    img: `${IMG_URL}maria.jpg`,
    mail: 'emma@radiodiodi.fi'
  },
  {
    name: 'Roope',
    role: 'Rakennusmestari',
    img: `${IMG_URL}roope.jpg`,
    mail: 'vili@radiodiodi.fi'
  },
  {
    name: 'Ville',
    role: 'Tekniikkavastaava',
    img: `${IMG_URL}kauha.jpg`,
    mail: 'jonatan@radiodiodi.fi'
  },
  {
    name: 'Ilari',
    role: 'Tekniikkavastaava',
    img: `${IMG_URL}ilari.jpg`,
    mail: 'severi@radiodiodi.fi'
  }
];

const ImageGallery = () => {
  return (
    <Gallery>
        {people.map((person, index) => (
          <Person key={index} {...person} />
        ))}
    </Gallery>
  );
}

export default ImageGallery;
