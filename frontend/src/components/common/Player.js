import React, { useState, useEffect, useRef, useContext, useCallback } from 'react';
import styled from 'styled-components';
import {
  fetchNowPlayingProgramme,
  environment
} from '../../utils';
import { SocketContext } from './SocketProvider';

import { Link as ReactLink } from 'react-router-dom';
import playIcon from '../../svg/play.svg';
import pauseIcon from '../../svg/pause.svg';
import volumeIcon from '../../svg/speaker.svg';

const siili = `${environment.REACT_APP_STATIC_URL}/img/2022/sponsors/Siili_white.png`;


const Container = styled.div`
  margin: 1rem 0 1rem;
  padding: 1rem 1rem;
  background-color: ${p => p.theme.color.white10};
  margin-bottom: 1rem;
`;

const PlayerContainer = styled.div`
  display: flex;
  flex: 3;
  flex-flow: row wrap;

  align-items: center;
  justify-content: space-around;
`;


const NowPlaying = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  & > strong {
    font-size: 14pt;
  }
`;

const PlayButton = styled.img`
  width: 48px;
  min-width: 48px;
  height: 48px;
  min-height: 48px;
  margin-right: 1.5rem;
  cursor: pointer;
`;

const PlayControl = styled.div`
  display: flex;
  flex-flow: row nowrap;
  margin: 1.5rem;
`;

const AudioElement = styled.audio`
  display: hidden;
`;

const NowPlayingValue = styled.i`
  display: block;
  line-height: 1.2rem;
  font-size: 10pt;
`;

const OwnPlayer = styled.div`
  text-align: right;
  font-size: 0.8rem;
  margin-bottom: -0.5rem;
`;

const VolumeSlider = styled.input`
  display: block;
  margin: 20px 0;
  height: 5px;
  width: 100%; /* width of play button rect */
  background: #222;
  border: none;

  -webkit-appearance: none;

  &::-webkit-slider-thumb {
    -webkit-appearance: none;
    border-radius: 50%;
    width: 20px;
    height: 20px;
    background: ${p => p.theme.color.realOrange};
    cursor: pointer;
    border: none;
  }

  &::-moz-range-thumb {
    border-radius: 50%;
    width: 20px;
    height: 20px;
    background: ${p => p.theme.color.realOrange};
    cursor: pointer;
    border: none;
  }

  &::-moz-range-progress {
    background: ${p => p.theme.color.realOrange}
  }
`;

const VolumeControl = styled.span`
  display: flex;
  flex-flow: row nowrap;
  max-width: 20rem;
  & > img {
    width: 24px;
    height: 45px;
    margin-right: 38px;
    margin-left: 12px;
  }
  
  @media screen and (max-width: 799px) {
    display: none !important;
  }
`;

const Sponsor = styled.img`
  max-height: 150px;
  max-width: 250px;
`;

const StreamSelector = styled.select`
  border: solid 2px ${p => p.theme.color.darkOrange};
  padding: 0.2em;
  
  font-size: 1rem;
  font-weight: 700;
  color: ${p => p.theme.color.realOrange};
  background-color: ${p => p.theme.color.bgGreen};

  max-width: max-content;
`;

const SettingsPanel = styled.div`
  display: ${p => p.hidden ? 'none' : 'flex'};
  flex-wrap: wrap;
  border-bottom: solid 1px white;
  padding: 1rem;

  align-items: center;
  justify-content: center;
  gap: 2rem;
`;

const SettingsButton = styled.button`
  float: right;
  max-width: max-content;
  border: none;
  background: unset;
  cursor: pointer;

  font-size: 1.5rem;
  color: ${p => p.theme.color.white70};

  &:hover {
    color: ${p => p.theme.color.darkOrange};
  }
`;

const Link = styled(ReactLink)`
  font-size: 1.0rem;
  font-weight: bold;
  color: ${p => p.color || p.theme.color.realOrange};
  margin-left: 0.5rem;
  transition: color 0.3s ease;

  &:hover, &:focus {
    color: ${p => p.theme.color.darkOrange};
    text-decoration: none;
  }
`;

const Player = () => {
  const audioRef = useRef(null)
  const connection = useContext(SocketContext);
  const [playing, setPlaying] = useState(false);
  const [programme, setProgramme] = useState();
  const [title, setTitle] = useState();
  const [artist, setArtist] = useState();
  const [settingsHidden, setSettingsHidden] = useState(true);

  const playPause = () => {
    if (!playing) {
      console.log("play", audioRef.current.src)
      audioRef.current.load();
      audioRef.current.play();
    } else {
      audioRef.current.pause();
    }
    setPlaying(!playing)
  };

  const switchAudioSrc = (event) => {
    audioRef.current.src = `${environment.REACT_APP_STREAM_URL}/${event.target.value}`;
    audioRef.current.load();
    audioRef.current.play();
    setPlaying(true)
  }
  
  const volumeChange = (event) => {
    audioRef.current.volume = event.target.value;
  };

  const clearSong = () => {
    setTitle(null);
    setArtist(null);
  };

  const handleSocketData = useCallback((event) => {
    const data = JSON.parse(event.data);

    if (data.song) {
      if (Object.keys(data.song).length === 0) {
        clearSong();
        return;
      }
      setTitle(data.song.title);
      setArtist(data.song.artist);
    }
  }, []);

  const fetchNowPlaying = async () => {
    let programme;
    try {
      programme = await fetchNowPlayingProgramme();
    } catch (error) {
      console.log(error);
    }

    if (!programme || !programme.title) {
      setProgramme(null);
      return;
    }

    setProgramme(programme.title);
  }

  const renderCurrentSong = () => {
    if (title && artist) {
      return (
        <NowPlayingValue>
          {artist} - {title}
        </NowPlayingValue>
      );
    } else {
      return null;
    }
  };

  const toggleSettings = () => {
    setSettingsHidden(!settingsHidden)
  }

  const icon = !playing ? playIcon : pauseIcon;
  const currentProgramme = programme || 'Radiodiodi';

  useEffect(() => {
    const fetchNowPlayingInterval = window.setInterval(fetchNowPlaying, 60000); // 60 seconds interval
    fetchNowPlaying();

    return () => {
      window.clearInterval(fetchNowPlayingInterval);
    };
  }, [])

  useEffect(() => {
    connection.addEventListener('message', handleSocketData);
    connection.addEventListener('error', clearSong);

    return () => {
      connection.removeEventListener('message', handleSocketData);
      connection.removeEventListener('error', clearSong);
    };
  }, [connection, handleSocketData]);

  return (
    <Container>
      <AudioElement preload="none" ref={audioRef}> 
        <source
          src={`${environment.REACT_APP_STREAM_URL}/mp3`}
          type="audio/mpeg"
        />
        <source
          src={`${environment.REACT_APP_STREAM_URL}/flac`}
          type="audio/flac"
        />
        <source
          src={`${environment.REACT_APP_STREAM_URL}/aac`}
          type="audio/aac"
        />
      </AudioElement>
      <SettingsButton onClick={toggleSettings} >
        <i className="fas fa-cog" />
      </SettingsButton>
      <SettingsPanel hidden={settingsHidden}>
        <StreamSelector onChange={switchAudioSrc}>
          <option value="mp3">
            MP3 256k
          </option>
          <option value="flac">
            FLAC
          </option>
          <option value="aac">
            AAC 64k
          </option>
        </StreamSelector>
        <VolumeControl>
          <img alt="Volume icon" src={volumeIcon} />
          <VolumeSlider type="range" min="0" max="1" step="0.01" defaulValue="1" onChange={volumeChange} />
        </VolumeControl>
        <OwnPlayer>
          Vaativalle kuulijalle:<Link to="/guide/faq">lisätietoa</Link>
        </OwnPlayer>
      </SettingsPanel>
      <PlayerContainer>
        <PlayControl>
          <PlayButton onClick={playPause} src={icon} />{' '}
          <NowPlaying>
            <strong>{currentProgramme}</strong>
            {renderCurrentSong()}
          </NowPlaying>
        </PlayControl>
        <Sponsor src={siili} alt=''/>
      </PlayerContainer>
    </Container>
  );
}

export default Player;
