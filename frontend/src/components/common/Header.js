import React, { useState } from 'react';
import styled from 'styled-components';
import { Link as ReactLink } from 'react-router-dom';
import Slogan from './Slogan';
import { linkToTelegramChat } from '../../utils';
import { environment } from '../../utils';

const logoURL = `${environment.REACT_APP_STATIC_URL}/img/ajaton/ajaton_header.svg`;

const HeaderContainer = styled.header`
  margin-bottom: 2rem;
`;

const HeaderNavContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-top: 0.3rem;
`;

const Logo = styled.img`
  max-width: 100%;
  height: auto;
  margin-bottom: -5%;

  @media screen and (max-width: 499px) {
    margin-bottom: 20px;
  }
`;


const SocialMediaButton = styled.i`
  font-size: 2rem;
`;

const SocialMediaLink = styled.a`
  color: ${p => p.theme.color.realOrange};
  transition: color 0.2s ease;

  &:hover, &:focus {
    color: ${p => p.theme.color.darkOrange};
  }

  margin: 0 0.5rem 0;
`;

const SocialMediaContainer = styled.span`
  display: flex;
  flex-flow: row nowrap;
  align-items: flex-end;
`;

const Link = styled(ReactLink)`
  font-size: 1.1rem;
  color: ${p => p.color || p.theme.color.lightOrange};
  margin-left: 0.5rem;
  transition: color 0.3s ease;

  &:hover, &:focus {
    color: ${p => p.theme.color.darkOrange};
    text-decoration: none;
  }
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const Menu = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media screen and (min-width: 1000px) {
  flex-direction: row;
  }
`;

const BurgerButton = styled.button`
  background: transparent;
  border: none;
  cursor: pointer;
  font-size: 1.8rem;
  padding: 0.5rem;
  margin-right: -0.5rem;
  color: ${p => p.active ? p.theme.color.realOrange : p.theme.color.white100};

  &:hover {
    color: ${p => p.theme.color.darkOrange};
  }

  @media screen and (min-width: 1000px) {
    display: none;
  }
`;

// const Freq = styled.div`
//   min-height: 2.5em;
//   margin: 0 0.3em 0
//   font-weight: bold;
//   font-size: 1.5rem;
//   line-height: 1em;
//   color: ${p => p.theme.color.orange100};

//   @media screen and (max-width: 499px) {
//     font-size: 1.3rem;
//   }
// `;

const Social = () => (
  <SocialMediaContainer>
    <SocialMediaLink target="_blank" rel="noopener noreferrer" title="Instagram" float href="https://instagram.com/radiodiodi">
      <SocialMediaButton className="fab fa-instagram" />
    </SocialMediaLink>
    <SocialMediaLink target="_blank" rel="noopener noreferrer" title="Facebook" href="https://www.facebook.com/radiodiodi">
      <SocialMediaButton className="fab fa-facebook" />
    </SocialMediaLink>
    <SocialMediaLink title="Telegram-keskustelu" onClick={linkToTelegramChat('radiodiodichat')}>
      <SocialMediaButton className="fab fa-telegram" />
    </SocialMediaLink>
    <SocialMediaLink title="Telegram-tiedotus" onClick={linkToTelegramChat('radiodiodi')}>
      <SocialMediaButton className="fab fa-telegram" />
    </SocialMediaLink>
  </SocialMediaContainer>
);

const Nav = styled.nav`
  display: ${p => p.expand ? "flex" : "none"};
  flex-direction: column;
  align-items: flex-end;
  line-height: 1.8;

  @media screen and (min-width: 1000px) {
    display: flex;
    flex-direction: row;
  }
`;

const renderNavigation = (expand) => (
  <Nav expand={expand}>
    <Link to="/">Etusivu</Link>
    {/* <Link to="/ilmo">Ilmoittaudu</Link> */}
    <Link to="/rekry">Toimittajaksi?</Link>
    <Link to="/library">Musiikkikirjasto</Link>
    <Link to="/for-companies">Yhteistyökumppaniksi?</Link>
    <Link to="/guide">Ohjelmantekijän&nbsp;opas</Link>
    <Link color="white" to="/english">In&nbsp;English</Link>
  </Nav>
);

const Header = () => {
  const [mobileMenuExpanded, setMobileMenuExpanded] = useState(false);

  const toggleMobileMenuExpanded = () => {
    setMobileMenuExpanded(!mobileMenuExpanded);
  }

  return (
    <HeaderContainer>
      <Link to="/">
        <Logo src={logoURL} width="3820" height="1200" />
      </Link>
      <Row>
        <Slogan />
        {/* <Freq>102,0 MHz</Freq> */}
      </Row>
      <HeaderNavContainer>
        <Menu>
          <Row>
            <Social />
            <BurgerButton active={mobileMenuExpanded} onClick={toggleMobileMenuExpanded}>
              <i className="fa fa-bars" />
            </BurgerButton>
          </Row>
          {renderNavigation(mobileMenuExpanded)}
        </Menu>
      </HeaderNavContainer>
    </HeaderContainer>
  );
}

export default Header;
