import React from 'react';
import styled from 'styled-components';

const phrases = [
  "Korkea taajuus, matala laatu",
  "Matala taajuus, korkea laatu",
  "Etelän halvinta puheaikaa",
  "Suomen nopein kuulotelevisio",
  "Ovi kiinni, Radio auki",
  "Oispa konteilla",
  "Mitäs tyhmää vois vielä keksiä...",
  "Ei mitään kakkaa vaan semi-laatua",
  "Buy high, sell low",
  "Älykkäät ihmiset, tyhmät järjestelmät",
  "Ai onks tää päällä huppista",
  "Se on rock!",
  "Testattu tuotannossa! :D",
  "Testataan tuotannossa! :P",
  "Sirkus pyörii, pellet vaihtuu",
  "Maistuu hyvältä kumpaankin suuntaan"
  //<>Tee ite paremmin:<a href="mailto:rekry@radiodiodi.fi">{' '}rekry@radiodiodi.fi</a></>
];

const getRandomPhrase = () => {
  return phrases[~~(Math.random() * phrases.length)];
}

const Phrase = styled.div`
  min-height: 2.5em;
  margin: 0 0.3em 0;

  font-size: 1.3rem;
  line-height: 1em;
  color: ${p => p.theme.color.realOrange};

  text-align: center;

  @media screen and (min-width: 499px) {
    max-width: 40%;
  }
`;

const phrase = getRandomPhrase();

const Slogan = () => {
  return (
      <Phrase>{phrase}</Phrase>
  )
}

export default Slogan;
