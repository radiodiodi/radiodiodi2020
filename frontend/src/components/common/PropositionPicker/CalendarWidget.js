import React, { Component } from 'react';
import styled from 'styled-components';

const Container = styled.div`
  text-align: center;
  padding: 0.5rem 1rem;
  background-color: ${p => p.theme.color.white10};
  margin-right: 0.5rem;

  @media screen and (min-width: 700px) {
    flex: 1 0 350px;
  }
`;

const Title = styled.h4`
  margin: 0;
`;

const DateContainer = styled.table`
  width: 100%;
  border-collapse: separate;
  border-spacing: 0 0.5rem;
  font-family: monospace;
  font-size: 1rem;
`;

const Date = styled.td`
  cursor: pointer;
  position: relative;

  &:hover:after {
    content: '';
    z-index: -1;
    position: absolute;

    height: 1.6em;
    width: 1.6em;
    border-radius: 100%;

    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    background-color: ${p => p.theme.color.darkOrange};
  }
`;

const DateHeader = styled.td`
  user-select: none;
  border-bottom: 1px solid;
`;

const FillerDate = styled.td`
`;

class CalendarWidget extends Component {
  renderDate = (date, index) => {
    const { onDateSelect } = this.props;
    if (!date) return <FillerDate />;
    return <Date key={index} onClick={() => onDateSelect(date)}>{date}</Date>
  }

  renderDateHeader = (date, index) => {
    return <DateHeader key={index}>{date}</DateHeader>
  }

  renderCalendar = () => {
    const weekdays = ['ma', 'ti', 'ke', 'to', 'pe', 'la', 'su'].map(this.renderDateHeader);
    const firstRow = ['', '', '', 14, 15, 16, 17].map(this.renderDate);
    const secondRow = [18, 19, 20, 21, 22, 23, 24].map(this.renderDate);
    const thirdRow = [25, 26, 27, 28, 29, 30, ''].map(this.renderDate);
    
    thirdRow[6] = '🥳'

    return (
      <Container>
        <Title>Huhtikuu</Title>
        <DateContainer>
          <thead>
            <tr>{weekdays}</tr>
          </thead>
          <tbody>
            <tr>{firstRow}</tr>
          </tbody>
          <tbody>
            <tr>{secondRow}</tr>
          </tbody>
          <tbody>
            <tr>{thirdRow}</tr>
          </tbody>
        </DateContainer>
      </Container>
    );
  }

  render() {
    const { show } = this.props;
    return show ? this.renderCalendar() : null;
  }
}

export default CalendarWidget;
