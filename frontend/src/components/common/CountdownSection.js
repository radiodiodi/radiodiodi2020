import React, { Component } from 'react';
import styled from 'styled-components';

import Countdown from './Countdown';

const SectionContainer = styled.div`
  padding: 8px 0 1.5rem;
  margin-bottom: 2rem;
  border-bottom: solid;
  border-width: 2px;
  border-color: ${p => p.theme.color.white100};

  text-align: center;
`;

const MonospaceSpan = styled.span`
  width: 15px;
  display: inline-block;
  font-size: 1.5rem;
  font-weight: bold;
`;

class CountdownSection extends Component {
  toMonospace(text) {
    return text
      .split('')
      .map((letter, i) => <MonospaceSpan key={i}>{letter}</MonospaceSpan>);
  }

  render() {
    return (
      <SectionContainer>
        <Countdown
          countTo={'Mon Apr 27 2020 9:00:00 GMT+0300 (EET)'}
          interval={1000}
          contentTransformFn={this.toMonospace}
        />
      </SectionContainer>
    );
  }
}

export default CountdownSection;
