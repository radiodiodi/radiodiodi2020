import React from 'react';
import styled from 'styled-components';
import { shortenText } from '../../utils'
//import placeholderImg from '../../images/placeholder_dark.svg'
import dateFormat from 'dateformat';
import { environment } from '../../utils';

const ProgramBlock = styled.div`
  padding: 1.5rem;
  min-height: ${p => p.maintainance ? 'none' : '150px'};

  &::after {
    content: "";
    clear: both;
    display: table;
  }

  background-color: ${p => p.theme.color.white10};
  margin-bottom: 0.5rem;
  display: flex;
  flex-direction: row;

  @media screen and (max-width: 500px) {
    flex-direction: column;
  }

  justify-content: space-between;

  @media screen and (max-width: 700px) {
    display: ${p => p.oneDayPreview ? 'none' : 'flex'};
  }
`;

const Column = styled.div`
  flex: ${p => p.flex};
  /* display: flex; */
  flex-direction: column;
`;

const Image = styled.img`
  height: 150px;
  width: 150px;
  background-size: cover;
  margin-right: 1.5rem;

  @media screen and (max-width: 500px) {
    margin: 0 2rem 2rem 0;
    align-self: flex-start;
  }
`;

const Title = styled.h4`
  margin: 0.5rem 0;
  font-size: 18px;
  border-bottom: 1px solid ${p => p.theme.color.orange100};
  word-wrap: break-word;

  @media (max-width: 600px) {
    margin-right: 0;
  }
`;

const Genre = styled.small`
  margin-top: -4px;
  color: #bbb;
  letter-spacing: 1px;
`;

const Author = styled.span`
  display: inline-block;
  margin: 0.5rem 0;
  font-weight: normal;
  font-size: 1rem;
`;

const Paragraph = styled.p`
  font-size: 14px;
  line-height: 1.3;
  margin: 0.5em 0;
  @media screen and (max-width: 500px) {
    font-size: 16px;
  }
`;

const ShowMore = styled.span`
  cursor: pointer;
  font-size: 12px;
  padding: 0;

  @media screen and (max-width: 800px - 1px) {
    font-size: 16px;
  }
`;

const PlaceholderImage = ({ alt }) => (
  <Image
    src={`${environment.REACT_APP_STATIC_URL}/img/2022/ohjelmat/placeholder.png`}
    alt={alt}
  />
);

const ProgramImage = ({ src, alt }) => {
  // const originalHref = src.replace('/ohjelmat', '/ohjelmat/original');

  return (
    // <a href={originalHref}>
    <Image
      src={src}
      alt={alt}
    />
    // </a>
  );
}

const AccentSpan = styled.span`
  color: ${p => p.theme.color.realOrange};
`;

const DESCRIPTION_MAX_LENGTH = 250;
const ExpandTextLink = ({ expandedText, toggleExpand }) => (
  <ShowMore onClick={toggleExpand}>
    {expandedText}
  </ShowMore>
);

class Program extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      expanded: false
    }
    this.toggleExpand = this.toggleExpand.bind(this);
  }

  toggleExpand() {
    this.setState({ expanded: !this.state.expanded });
  }

  renderDescription = () => {
    const { p } = this.props;
    const { expanded } = this.state;

    const expandedText = (
      <AccentSpan>
        {expanded ? ' Vähemmän' : ' Lisää'}
      </AccentSpan>
    );

    const expandLink = p.description.length > DESCRIPTION_MAX_LENGTH
      ? <ExpandTextLink expandedText={expandedText} toggleExpand={this.toggleExpand} />
      : null;

    return (
      <Paragraph>
        {this.state.expanded ? p.description : shortenText(p.description, DESCRIPTION_MAX_LENGTH)}
        {expandLink}
      </Paragraph>
    );
  }

  render() {
    const { p, oneDayPreview } = this.props;
    const maintainance = p.title === 'HUOLTOTAUKO';
    const img = p.image && p.image.trim()
      ? <ProgramImage src={p.image} alt={p.title} />
      : <PlaceholderImage alt={p.title} />;

    const startDate = dateFormat(p.start, 'HH:MM');
    const endDate = dateFormat(p.end, 'HH:MM');

    const description = p.description
      ? this.renderDescription()
      : null;

    return (
      <ProgramBlock oneDayPreview={oneDayPreview} maintainance={maintainance}>
        <Column>
          {img}
        </Column>
        <Column flex={1}>
          <small>{`${startDate} - ${endDate}`}</small>
          <Title>{p.title} - <Author>{p.team}</Author></Title>

          {description}
          <Genre>{p.genre}</Genre>
        </Column>
      </ProgramBlock>
    )
  }
}

export default Program
