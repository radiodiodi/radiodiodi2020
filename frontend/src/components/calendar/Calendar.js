import React, { useState, useEffect} from 'react';
import styled from 'styled-components'
import Program from './Program'
import { environment } from '../../utils';

const { 
  REACT_APP_BACKEND_HTTP_URL,
  REACT_APP_CALENDAR_ID,
  REACT_APP_START_DATE,
  REACT_APP_END_DATE
} = environment;

const startDate = new Date(REACT_APP_START_DATE)
const endDate = new Date(REACT_APP_END_DATE)

const Button = styled.button`
  background-color: ${p => p.theme.color.realOrange};
  color: black;
  padding: 0.5rem 0;
  font-size: 14px;
  border: none;
  min-width: 100px;
  cursor: pointer;
  
  &:hover, &:focus {
    background-color: ${p => p.theme.color.darkOrange};
  }

  @media screen and (max-width: 450px) {
    margin: 0.5rem 0;
  }
`;

const Controls = styled.div`
  margin: 1rem 0;
  padding: 0.5rem 0;
  text-align: center;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  flex-direction: row;
  align-items: center;

  @media screen and (max-width: 450px) {
    flex-direction: column;
  }
`;

const CalendarLink = styled.div`
  text-align: center;
  margin-bottom: 2rem;
`;

const groupBy = (xs, key) => xs
  .reduce((rv, x) => {
    var v = key instanceof Function ? key(x) : x[key];
    (rv[v] = rv[v] || []).push(x);
    return rv;
  }, {});

  const preloadImages = async (images) => {
    images.forEach((src) => {
      const img = document.createElement('img');
      img.src = src; // Assigning the img src immediately requests the image
    });
  }

const currentDateWithinBounds = () => startOfDay(Math.min(endDate, Math.max(new Date(), startDate)))

const startOfDay = (date) => new Date(date).setHours(0,0,0,0);

const Calendar = ({ oneDayPreview }) =>  {
  const [all, setAll] = useState()
  const [today, setToday] = useState()

  useEffect((() => {
    if (Date.parse(REACT_APP_START_DATE) && Date.parse(REACT_APP_END_DATE)) {
      const fetchProgrammes = async () => {
        try {
          const resp = await fetch(`${REACT_APP_BACKEND_HTTP_URL}/programmes`)
          const data = await resp.json();
          if (!data || !Array.isArray(data)) {
            console.error('Programme data invalid. Data: ', data);
            return;
          }

          const images = data.map(p => p.image);
          preloadImages(images);

          data.forEach(program => {
            program.start = new Date(program.start);
            program.end = new Date(program.end);
          })

          const r = data.sort((x, y) => + x.start - y.start);
          const grouped = groupBy(r, (x) => startOfDay(x.start));

          setToday(currentDateWithinBounds())
          setAll(grouped)
        } catch (error) {
          console.error(error);
        }
      }
      fetchProgrammes();
    }
  }), [])

  const incrementDay = () => {
    setToday(startOfDay(Math.min(today + 86400000, endDate)))
  }

  const decrementDay = () => {
    setToday(startOfDay(Math.max(today - 86400000, startDate)))
  }

  if (!all || !today) return null;

  const dateOptions = { weekday: 'long', year: 'numeric', month: 'numeric', day: 'numeric' };
  const date = new Date(today).toLocaleDateString('fi-FI', dateOptions);
  const calendarControls = <Controls>
    <Button onClick={decrementDay}>Edellinen</Button>
    <span>{date}</span>
    <Button onClick={incrementDay}>Seuraava</Button>
  </Controls>

  if (oneDayPreview) {
    if (all[today]) {
      const currentProgram = all[today].find(program => {
        const now = new Date();
        const start = program.start;
        const end = program.end;
        const hasNotEnded = end > now;
        const hasStarted = start < now;
        return hasStarted && hasNotEnded;
      });

      if (!currentProgram) {
        return null;
      }
      return <Program oneDayPreview p={currentProgram} />;
    } else {
      return null;
    }
  }

  const content = all[today]
    ? all[today].map(p => <Program p={p} key={p.id || p.start} />)
    : [];

  return (
    <div>
      <h2 id="calendar">Ohjelmakalenteri</h2>
      {calendarControls}
      {content}
      {calendarControls}
      <CalendarLink>
        Kalenteri on luettavissa kokonaisuudessaan täällä:&nbsp;
        <a href={`https://calendar.google.com/calendar/b/4/r?cid=${REACT_APP_CALENDAR_ID}`}>
          Google Calendar
        </a>
      </CalendarLink>
    </div>
  )
}

export default Calendar
