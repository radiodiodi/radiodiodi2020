import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Header from '../common/Header';
import Footer from '../common/Footer';
import Background from '../common/Background';
import Frontpage from '../pages/frontpage/Frontpage';
import Companies from '../pages/sponsors/Companies';
import Registration from '../pages/registration/Registration';
import Library from '../pages/library/Library';
import Login from '../pages/admin/Login';
import Guide from '../pages/guide/Guide';
import English from '../pages/english/English';
// import CalendarCallback from '../pages/calendar-callback/CalendarCallback';
import RegistrationSuccess from '../pages/registration/Success';
import RegistrationError from '../pages/registration/Error';
// import AdminRegistrationsPage from '../pages/admin/AdminRegistrationsPage';
// import AdminShoutboxPage from '../pages/admin/AdminShoutboxPage';
// import AdminBansPage from '../pages/admin/AdminBansPage';
// import AdminReservedPage from '../pages/admin/AdminReservedPage';
// import Player from '../common/Player'
// import Shoutbox from '../common/Shoutbox'
import ChatProvider from '../common/ChatProvider';
import Recruitment from '../pages/recruitment/Recruitment';
import RecruitmentSuccess from '../pages/recruitment/Success';
import RecruitmentError from '../pages/recruitment/Error';

export default () => {
  return (
    <Router>
      <Switch>
        <ChatProvider>
          {/* <Route exact path="/huutis" component={Shoutbox} /> */}
          <Route path="/" >
            <Background />
            <Header />
            {/* <Player /> */}
            <Switch>
              <Route exact path="/" component={Frontpage} />
              <Route path="/for-companies" component={Companies} />
              <Route path="/ilmo/success" component={RegistrationSuccess} />
              <Route path="/ilmo/error" component={RegistrationError} />
              <Route path="/ilmo" component={Registration} />
              <Route path="/guide" component={Guide} />
              {/* <Route path="/admin/bans" component={AdminBansPage} />
              <Route path="/admin/registrations" component={AdminRegistrationsPage} />
              <Route path="/admin/reserved_users" component={AdminReservedPage} /> */}
              {/* <Route path="/admin/shoutbox" component={AdminShoutboxPage} /> */}
              <Redirect from="/admin" to="/admin/shoutbox" />
              <Route path="/login" component={Login} />
              <Route path="/library" component={Library} />
              <Route path="/rekry/success" component={RecruitmentSuccess} />
              <Route path="/rekry/error" component={RecruitmentError} />
              <Route path="/rekry" component={Recruitment} />
              <Route path="/english" component={English} />
              {/* <Route path="/callback" component={CalendarCallback} /> */}
              {/* <Route path="/shoutbox" component={Shoutbox} /> */}
              <Route component={() => <h1>404</h1>} />
            </Switch>
          </Route>
        </ChatProvider>
      </Switch>
      <Footer />
    </Router>
  );
}
