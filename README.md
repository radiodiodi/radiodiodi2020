# Radiodiodi 2020

[![pipeline status](https://gitlab.com/radiodiodi/radiodiodi2020/badges/master/pipeline.svg)](https://gitlab.com/radiodiodi/radiodiodi2020/commits/master)

## Build

To trigger an automatic build just tag a commit an push the tag (and commit) to GitLab. This will build two docker images (frontend & backend) with the same tag and publish them in the [container registry](https://gitlab.com/radiodiodi/radiodiodi2020/container_registry) for this repository.

To tag a commit for deployment use an annotated tag
```bash
git tag -a <tag>
```

Then push the commit and the tag

```bash
git push --follow-tags

# or push the tag separately
git push origin <tag>
```

Wait for pipeline to finish. There should be new container image in container registry.

## Deploy
The deployment is done manually to Kubernetes with the configuration in the [k8s-conf](https://gitlab.com/radiodiodi/k8s-conf) repository.

Modify the image tags in the web-frontend.yaml and web-backend.yaml files and apply the changes with: ```kubectl apply -f <file / folder>```

For more info on the deployment consult the [README](https://gitlab.com/radiodiodi/k8s-conf/-/blob/master/README.md) in the k8s-conf repo.
