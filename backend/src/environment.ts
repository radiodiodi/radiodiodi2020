// This file handles environments with Docker secrets.
// Since Docker cannot inject secrets straight into ENV vars,
// we read the secret files and create new environment object.

import fs from 'fs';
import dotenv from 'dotenv';

dotenv.config();

const secretFileString = '/run/secrets';

const secrets: Record<string, string | undefined> = {
  INSTAGRAM_USER_ID: process.env.INSTAGRAM_USER_ID && process.env.INSTAGRAM_USER_ID.startsWith(secretFileString) ? fs.readFileSync(process.env.INSTAGRAM_USER_ID, 'utf8').trim() : process.env.INSTAGRAM_USER_ID,
  INSTAGRAM_ACCESS_TOKEN: process.env.INSTAGRAM_ACCESS_TOKEN && process.env.INSTAGRAM_ACCESS_TOKEN.startsWith(secretFileString) ? fs.readFileSync(process.env.INSTAGRAM_ACCESS_TOKEN, 'utf8').trim() : process.env.INSTAGRAM_ACCESS_TOKEN,
  RADIODJ2_AUTH_SECRET: process.env.RADIODJ2_AUTH_SECRET && process.env.RADIODJ2_AUTH_SECRET.startsWith(secretFileString) ? fs.readFileSync(process.env.RADIODJ2_AUTH_SECRET, 'utf8').trim() : process.env.RADIODJ2_AUTH_SECRET,
  SONG_UPDATE_TOKEN: process.env.SONG_UPDATE_TOKEN && process.env.SONG_UPDATE_TOKEN.startsWith(secretFileString) ? fs.readFileSync(process.env.SONG_UPDATE_TOKEN, 'utf8').trim() : process.env.SONG_UPDATE_TOKEN,
};

const environment = {
  ...process.env,
  ...secrets,
};

export default environment;
