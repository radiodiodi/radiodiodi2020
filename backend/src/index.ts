import Koa from 'koa';
import koaBody from 'koa-body';
import cors from '@koa/cors';
import promClient from 'prom-client';
import environment from './environment';
import logger from './logger';
import router from './router';
import websockets from './websockets';

promClient.collectDefaultMetrics({ prefix: 'diodi_' });

const app = new Koa();
app.use(koaBody({ jsonLimit: '5mb', includeUnparsed: true }));
app.proxy = true;

const { FRONTEND_URL, CALENDAR_ID, PORT, HOST } = environment;

// x-response-time
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

// logger
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  if (ctx.status < 400) {
    // logger.info(`${ctx.status} ${ctx.method} ${ctx.url} - ${ms}ms`);
  } else {
    logger.error(`${ctx.status} ${ctx.method} ${ctx.url} - ${ms}ms`);
  }
});

websockets.start();

logger.info(`Using calendar ID ${CALENDAR_ID}`);
logger.info(`Listening for HTTP on ${HOST} on port ${PORT}.`);

app
  .use(cors({
    origin: FRONTEND_URL,
  }))
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(PORT);
