import geoip from 'geoip-lite';
import logger from './logger';
import models from './models';
import metrics from './prom-metrics';
import { Message } from './websockets';

const MAX_MESSAGE_LENGTH = 500;
const MAX_USERNAME_LENGTH = 16;

/*
 * A good filter:
 *  - Returns message object if message is valid
 *  - Returns null if not
 *  - Lets messages pass by default, if there is problem in filer execution
 */
type ErrorSender = (msg: string) => void;
type FilterFunction = (message: Message, errorSender: ErrorSender) => Promise<Message | null>;

const disableChatFilter: FilterFunction = async (message, errorSender) => {
  errorSender('Chat is not available at the moment');
  metrics.messagesDroppedCounter.labels('chat_disabled').inc();
  return null;
};

const reservedFilter: FilterFunction = async (message, errorSender) => { // eslint-disable-line
  let reserved = false;
  try {
    reserved = (await models.reserved.count({ ip: message.ip })) !== 0;
  } catch (error) {
    logger.warning(`Error when querying reserved ips: ${error}`);
  }
  return { ...message, reserved };
};

const messageLengthFilter: FilterFunction = async (message, errorSender) => {
  if (message.text.length > MAX_MESSAGE_LENGTH) {
    errorSender(`Message too long. Max length is ${MAX_MESSAGE_LENGTH} characters.`);
    metrics.messagesDroppedCounter.labels('message_too_long').inc();
    return null;
  }
  return message;
};

const usernameLengthFilter: FilterFunction = async (message, errorSender) => {
  if (message.name.length > MAX_USERNAME_LENGTH) {
    errorSender(`Username too long. Max length is ${MAX_USERNAME_LENGTH} characters.`);
    metrics.messagesDroppedCounter.labels('username_too_long').inc();
    return null;
  }
  return message;
};

const bannedIpFilter: FilterFunction = async (message, errorSender) => {
  try {
    const banned = await models.bans.findOne({ ip: message.ip });
    if (banned) {
      errorSender('You are banned.');
      metrics.messagesDroppedCounter.labels('user_banned').inc();
      return null;
    }
  } catch (error) {
    logger.warning(`Error when querying banned ips: ${error}`);
  }
  return message;
};

const bannedWordFilter: FilterFunction = async (message, errorSender) => {
  try {
    // TODO: Better type
    const bannedWords: any = await models.bannedWords.find({});
    for (const entry of bannedWords) {
      if (message.text.includes(entry.word)) {
        errorSender('You said something naughty.');
        metrics.messagesDroppedCounter.labels('bad_word');
        return null;
      }
    }
  } catch (error) {
    logger.warning(`Error when querying banned words: ${error}`);
  }
  return message;
};

const geoblockFilter: FilterFunction = async (message, errorSender) => {
  if (message.reserved) return message;
  if (message.ip === undefined) return message;
  const lookupResult = geoip.lookup(message.ip);
  const inFinland = lookupResult && lookupResult.country === 'FI';
  if (!inFinland && message.ip !== '127.0.0.1') {
    logger.info(`Dropping message from IP ${message.ip} based on geoip lookup`);
    errorSender('Messages from outside of Finland not allowed to deter spamming.');
    metrics.messagesDroppedCounter.labels('geoblock').inc();
    return null;
  }
  return message;
};

const filters = [
  {
    slug: 'chat-disabled',
    description: 'Disable chat completely',
    order: 1,
    defaultState: true,
    code: disableChatFilter,
  }, {
    slug: 'reserved-filter',
    description: 'Verify reserved users.',
    order: 3,
    defaultState: true,
    code: reservedFilter,
  }, {
    slug: 'geoblock',
    description: 'Drop messages originating from abroad',
    order: 5,
    defaultState: false,
    code: geoblockFilter,
  }, {
    slug: 'banned-ip',
    description: 'Check user ip against banned ips',
    order: 10,
    defaultState: true,
    code: bannedIpFilter,
  }, {
    slug: 'message-length',
    description: 'Limit message length',
    order: 20,
    defaultState: true,
    code: messageLengthFilter,
  }, {
    slug: 'username-length',
    description: 'Limit username length',
    order: 30,
    defaultState: true,
    code: usernameLengthFilter,
  }, {
    slug: 'banned-word',
    description: 'Drop messages containing banned words',
    order: 50,
    defaultState: true,
    code: bannedWordFilter,
  },
];

export default filters;
