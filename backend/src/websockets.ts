import WebSocket from 'ws';
import rateLimiter from 'ws-rate-limit';
import keyBy from 'lodash/keyBy';
import models from './models';
import logger from './logger';
import utils from './utils';
import type { SongData } from './utils';
import environment from './environment';

import {
  messagesReceivedCounter,
  messagesDroppedCounter,
  messagesRelayedHistogram,
  wsCurrentClients,
  wsErrorsCounter,
} from './prom-metrics';

import filterData from './filters';

export type Message = {
  timestamp: Date;
  name: string;
  text: string;
  ip: string | undefined;
  reserved: boolean;
}

const filters = keyBy(filterData, 'slug');

const initializeFilters = async () => {
  const docs = await models.filters.find({}, { fields: { slug: 1 } });
  const slugsInDB = docs.map(d => d.slug);
  const slugsInCode = filterData.map(f => f.slug);

  // remove dangling slugs from database
  const danglingSlugs = slugsInDB.filter(slug => !slugsInCode.find(s => s === slug));
  danglingSlugs.forEach(slug => {
    models.filters
      .remove({ slug })
      .then(() => logger.info(`Removed dangling filter with slug ${slug}`))
      .catch(err => logger.error(err));
  });

  // insert new filters to database
  const slugsToInsert = slugsInCode.filter(slug => !slugsInDB.find(s => s === slug));
  slugsToInsert.forEach(slug => {
    const filter = filterData.find(f => f.slug === slug);
    if (!filter) {
      logger.error('Filter not found');
      return;
    }
    const { order, description, defaultState } = filter;
    models.filters
      .insert({
        slug,
        order,
        description,
        state: defaultState,
      })
      .then(() => logger.info(`Inserted filter to database with slug ${slug}`))
      .catch(err => logger.error(err));
  });
};

/* Timeout counter for resetting the current song in frontend
 * (after the song ends, if no new song is POSTed from RadioDJ)
 */

let currentSongTimeout: ReturnType<typeof setTimeout> | undefined;

// Song data for the currently playing song

let currentSong: SongData | undefined;

let wss: WebSocket.Server;

const sendForEachClient = (data: object) => {
  wss.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify(data));
    }
  });
};

const eraseMessage = (id: string) => sendForEachClient({ erase: id });

// Update song for each WebSocket client

const sendSong = (songData: SongData) => sendForEachClient({ song: songData });

const clearCurrentSongOnTimeout = (time: number) => {
  currentSongTimeout = setTimeout(() => {
    currentSong = undefined;
    sendSong({});
  }, time);
};

const clearSongTimeout = () => {
  if (currentSongTimeout) {
    clearTimeout(currentSongTimeout);
  }
};

const updateSong = (songData: SongData) => {
  const remaining: number | null = utils.calculateSongRemainingTime(songData);
  if (remaining) {
    clearSongTimeout();
    sendSong(songData);
    currentSong = songData;
    clearCurrentSongOnTimeout(remaining + 5000); // remaining time + 5s
  }
};

const start = async () => {
  logger.info(`Listening for Websockets on ${environment.HOST} on port ${environment.WS_PORT}.`);
  initializeFilters();

  // Get currently playing song from DB
  const initialSong: SongData = await utils.getCurrentSongFromDb();

  wss = new WebSocket.Server({
    host: environment.HOST,
    port: Number(environment.WS_PORT),
  });
  const rateLimit = rateLimiter('60s', 3);

  // Set current song and clear on timeout
  if (Object.keys(initialSong).length > 0) {
    const remaining: number | null = utils.calculateSongRemainingTime(initialSong);
    if (remaining) {
      clearSongTimeout();
      currentSong = initialSong;
      clearCurrentSongOnTimeout(remaining + 5000); // remaining time + 5
    }
  } else currentSong = undefined;

  wss.on('connection', async (ws, req) => {
    const forwardedIP = req.headers['x-forwarded-for'] as string;
    const ip = forwardedIP && forwardedIP !== '127.0.0.1' ? forwardedIP : req.connection.remoteAddress;
    const reserved = (await models.reserved.count({ ip })) !== 0;
    if (!reserved) rateLimit(ws);

    ws.on('message', async data => {
      messagesReceivedCounter.inc();
      const timerStop = messagesRelayedHistogram.startTimer();
      const { name, text } = JSON.parse(data as string);
      // logger.info(`Websocket: Received: "${text}" from "${name}"`);

      let message: Message | null = {
        timestamp: new Date(Date.now()),
        name,
        text,
        ip,
        reserved,
      };

      const errorSender = (errorMessage: string) => {
        ws.send(JSON.stringify({
          message: {
            name: 'SERVER',
            error: true,
            timestamp: new Date(Date.now()),
            text: errorMessage,
          },
        }));
      };

      try {
        const activeFilters = await models.filters.find(
          { state: true },
          { fields: { slug: 1 }, sort: { order: 1 } },
        );
        const slugs = activeFilters.map(f => f.slug);
        for (const slug of slugs) {
          const filter = filters[slug].code;
          message = await filter(message, errorSender);
          if (!message) return;
        }
      } catch (err) {
        logger.error(`Error in message filtering: ${err}`);
        if (!message) return;
      }

      // perform final sanity check
      if (typeof (message.timestamp) !== 'object'
          || typeof (message.text) !== 'string'
          || typeof (message.name) !== 'string'
          || typeof (message.ip) !== 'string'
      ) {
        console.error(`Dropped invalid message object: ${message}`);
        return;
      }

      const dbMessage = await models.messages.insert(message);

      sendForEachClient({
        message: {
          name: message?.name,
          text: message?.text,
          timestamp: message?.timestamp,
          _id: dbMessage._id,
          reserved: message?.reserved,
        },
      });
      timerStop();
    });

    const initial = (await models.messages.find({}, {
      sort: { timestamp: -1 }, limit: 100,
    })).reverse();

    ws.send(JSON.stringify({
      initial: initial.map(message => ({
        name: message.name,
        text: message.text,
        timestamp: message.timestamp,
        _id: message._id,
        reserved: message.reserved,
      })),
    }));

    // Send current song data on WebSocket connection

    ws.send(JSON.stringify({
      song: currentSong ?? {},
    }));

    ws.on('error', error => {
      wsErrorsCounter.inc();
      logger.error(`Websocket error. ${error}`);
    });

    ws.on('limited', () => {
      messagesReceivedCounter.inc();
      messagesDroppedCounter.labels('limited').inc();
      logger.warning(`User from ip "${req.connection.remoteAddress}" has been throttled.`);
      ws.send(JSON.stringify({
        message: {
          name: 'SERVER',
          text: 'Calm down, you are sending too many messages.',
          timestamp: new Date(Date.now()),
          error: true,
        },
      }));
    });
  });

  // update client count every 10 seconds
  setInterval(() => wsCurrentClients.set(Number(wss.clients.size)), 10000);
};

export default {
  start,
  eraseMessage,
  updateSong,
};
