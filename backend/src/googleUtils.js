import fs from 'fs';
import path from 'path';
import readline from 'readline';
import ejs from 'ejs';
import { google } from 'googleapis';
import { emailSentCounter, emailErrorCounter } from './prom-metrics';
import logger from './logger';
import environment from './environment';
import dayjs from 'dayjs';

const SCOPES = [
  'https://www.googleapis.com/auth/calendar.events',
  'https://www.googleapis.com/auth/gmail.send',
  'https://www.googleapis.com/auth/spreadsheets',
];
const TOKEN_DIR = path.join(__dirname, '..', '.credentials');
const TOKEN_PATH = path.join(TOKEN_DIR, 'radiodiodi-nodejs-credentials.json');
const { CALENDAR_ID, START_DATE, END_DATE, PROGRAM_EXCEL_ID, CALENDAR_EVENT_DATETIME} = environment;
const CALENDAR_INTERVAL = 1000 * 60 * 15; // 15 minutes

let calendarData = [];
let googleCredentials = null;

/* Parse event data from Google Calendar's event description.
   The editor must use this exact syntax:

   name
   ---
   description
   ---
   requires own producer? (needed only for editor's own notekeeping)
   ---
   genre/type of show
   ---
   full image link (https://.../image.png)
*/
function parseEventDescription(event) {
  const parts = event.description.split('---').map(p => p.replace(/\r?\n?/g, ''));
  return {
    ...event,
    team: parts[0],
    description: parts[1],
    genre: parts[3],
    image: parts[4],
  };
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
const storeToken = (token) => {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code !== 'EEXIST') {
      throw err;
    }
  }
  try {
    fs.writeFileSync(TOKEN_PATH, JSON.stringify(token));
    console.log(`Token stored to ${TOKEN_PATH}`);
  } catch (err) {
    console.error(err);
  }
};

const getNewToken = (oauth2Client) => new Promise((resolve, reject) => {
  const authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    prompt: 'consent',
    scope: SCOPES,
  });
  console.log('\nAuthorize this app by visiting this url: ', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('\nEnter the code from that page here: ', (code) => {
    rl.close();
    oauth2Client.getToken(code, (err, token) => {
      if (err) {
        console.error('Error while trying to retrieve access token', err);
        reject(err);
      }
      oauth2Client.credentials = token; // eslint-disable-line no-param-reassign
      storeToken(token);
      resolve(oauth2Client);
    });
  });
});

// eslint-disable-next-line
const authorize = (credentials) => new Promise(async (resolve, reject) => {
  const clientSecret = credentials.web.client_secret;
  const clientId = credentials.web.client_id;
  const redirectUrl = credentials.web.redirect_uris[0];

  let oauth2Client;
  try {
    oauth2Client = new google.auth.OAuth2(clientId, clientSecret, redirectUrl);
  } catch (err) {
    console.error(err);
    reject(err);
  }

  // Check if we have previously stored a token.
  try {
    const token = fs.readFileSync(TOKEN_PATH);
    oauth2Client.credentials = JSON.parse(token);
    resolve(oauth2Client);
  } catch (err) {
    console.error(err);
    resolve(getNewToken(oauth2Client));
  }
});

const fetchCalendarEvents = (payload, auth) => new Promise((resolve, reject) => {
  const calendar = google.calendar({ version: 'v3', auth });
  calendar.events.list(payload, (err, res) => {
    if (err) reject(err);
    else resolve(res);
  });
});

/**
 * Lists the next 10 events on the user's primary calendar.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
const listEvents = async (auth) => {
  const payload = {
    calendarId: CALENDAR_ID,
    timeMin: START_DATE,
    timeMax: END_DATE,
    singleEvents: true,
    orderBy: 'startTime',
  };

  let response;
  try {
    response = await fetchCalendarEvents(payload, auth);
  } catch (err) {
    console.error(err);
    return [];
  }

  const events = response.data.items || [];

  const newCalendar = events.map(event => {
    const title = event.summary;
    const description = event.description || '';
    const start = event.start.dateTime || event.start.date;
    const end = event.end.dateTime || event.end.date;
    const id = event.id || null;
    const result = parseEventDescription({
      id,
      title,
      description,
      start,
      end,
    });
    return result;
  });

  return newCalendar;
};

const initCalendarReader = () => {
  const readCalendar = async () => {
    try {
      const client = await authorize(googleCredentials);
      const results = await listEvents(client);

      calendarData = results;
      logger.info(`Updated calendar. Fetched ${results.length} results.`);
    } catch (err) {
      console.error(err);
      logger.error('Error while authorizing calendar.');
    }
  };

  readCalendar();

  setInterval(readCalendar, CALENDAR_INTERVAL);
};

const makeBody = ({ to, cc, bcc, from, subject, message }) => {
  const str = ['Content-Type: text/html; charset="UTF-8"\n',
    'MIME-Version: 1.0\n',
    'Content-Transfer-Encoding: 7bit\n',
    'To: ', to, '\n',
    'Cc: ', cc, '\n',
    'Bcc: ', bcc, '\n',
    'From: ', from, '\n',
    'Subject: ', subject, '\n\n',
    message,
  ].join('');

  const encodedMail = Buffer.from(str).toString('base64').replace(/\+/g, '-').replace(/\//g, '_');
  return encodedMail;
};

const registrationHtmlTemplate = fs.readFileSync('./resource/registration_email.html', 'utf8');
export const sendRegistrationGmailConfirmation = async (formParams) => {
  const html = ejs.render(registrationHtmlTemplate, { data: formParams });
  const data = {
    to: formParams.email,
    bcc: 'ohjelmantekijailmot@radiodiodi.fi',
    from: 'radiodiodi@radiodiodi',
    subject: 'Radiodiodi - Kiitos ilmoittautumisesta!',
    message: html,
  };

  const raw = makeBody(data);
  const auth = await authorize(googleCredentials);
  const gmail = google.gmail({ version: 'v1', auth });

  const payload = {
    auth,
    userId: 'me',
    resource: {
      raw,
    },
  };

  gmail.users.messages.send(payload, (err) => {
    if (err) {
      emailErrorCounter.inc();
      logger.error(`The Gmail API returned an error: ${err}`);
    } else {
      emailSentCounter.inc();
      logger.info(`Successfully sent email through Gmail API to ${data.to}`);
    }
  });
};

const recruitmentHtmlTemplate = fs.readFileSync('./resource/recruitment_email.html', 'utf8');
export const sendRecruitmentGmailConfirmation = async (formParams) => {
  const html = ejs.render(recruitmentHtmlTemplate, { data: formParams });
  const data = {
    to: formParams.email,
    bcc: 'rekry@radiodiodi.fi',
    from: 'radiodiodi@radiodiodi',
    subject: 'Radiodiodi - Kiitos hakemuksesta!',
    message: html,
  };

  const raw = makeBody(data);
  const auth = await authorize(googleCredentials);
  const gmail = google.gmail({ version: 'v1', auth });

  const payload = {
    auth,
    userId: 'me',
    resource: {
      raw,
    },
  };

  gmail.users.messages.send(payload, (err) => {
    if (err) {
      emailErrorCounter.inc();
      logger.error(`The Gmail API returned an error: ${err}`);
    } else {
      emailSentCounter.inc();
      logger.info(`Successfully sent email through Gmail API to ${data.to}`);
    }
  });
};

const formatPropositions = (propositions) => propositions.reduce((prev, curr) => {
  const newProposition = `(${curr.date} ${curr.startTime}-${curr.endTime})`;
  return prev ? `${prev}, ${newProposition}` : newProposition;
}, '');

const formatFormParams = (params) => {
  const {
    name,
    team,
    description,
    genre,
    responsible,
    producer,
    email,
    info,
    participants,
    duration,
    propositions } = params;

  const formattedPropositions = propositions ? formatPropositions(propositions) : null;

  return [
    name,
    team,
    description,
    genre,
    responsible,
    producer,
    email,
    info,
    participants,
    duration,
    formattedPropositions,
  ];
};

/**
 * Uploads the registration info to a Google spreadsheet
 */
export const uploadToProgramSpreadsheets = async (formParams) => {
  const sheets = google.sheets('v4');
  const formattedFormParams = formatFormParams(formParams);
  const auth = await authorize(googleCredentials);
  const values = [formattedFormParams];
  const request = {
    spreadsheetId: PROGRAM_EXCEL_ID,
    range: 'ilmot!A1',
    valueInputOption: 'USER_ENTERED',
    insertDataOption: 'INSERT_ROWS',
    resource: {
      values,
    },
    auth,
  };

  try {
    await sheets.spreadsheets.values.append(request);
  } catch (err) {
    console.error(err);
    logger.error('Error uploading registration to Excel');
  }
  logger.info('Uploaded data to the program excel.');
};


/**
 * Creates a calendar event for a registration
 */
 export const createProgramCalendarEvent = async (formParams) => {
  if (!dayjs(CALENDAR_EVENT_DATETIME).isValid()) {
    logger.info('Invalid event datetime. Did not create event')
    return
  }
  const auth = await authorize(googleCredentials);
  const calendar = google.calendar({ version: 'v3', auth });

  const {
    name,
    team,
    description,
    genre,
    duration,
  } = formParams;

  const endDateString = dayjs(CALENDAR_EVENT_DATETIME).add(duration, 'hour').toISOString()

  var event = {
    'summary': name,
    'description': `${team}\n---\n${description}\n---\n\n---\n${genre}\n---\n`,
    'start': {
      'dateTime': CALENDAR_EVENT_DATETIME,
      'timeZone': 'Europe/Helsinki'
    },
    'end': {
      'dateTime': endDateString,
      'timeZone': 'Europe/Helsinki'
    },
  };
  
  const request = {
    calendarId: CALENDAR_ID,
    requestBody: event,
  };

  try {
    const res = await calendar.events.insert(request);
    if (res.status !== 200){
      console.error(res)
    }
  } catch (err) {
    console.error(err);
    logger.error('Error creating new calendar event');
  }
  logger.info(`Created calendar event with name "${name}".`);
};

const clientSecretFilePath = 'client_secret.json';
if (environment.GOOGLE_CLIENT_SECRET) {
  fs.writeFileSync(clientSecretFilePath, environment.GOOGLE_CLIENT_SECRET);
}

if (environment.GOOGLE_CLIENT_CREDENTIALS) {
  if (!fs.existsSync(TOKEN_DIR)) {
    fs.mkdirSync(TOKEN_DIR);
  }
  fs.writeFileSync(TOKEN_PATH, environment.GOOGLE_CLIENT_CREDENTIALS);
}

googleCredentials = JSON.parse(fs.readFileSync(clientSecretFilePath));
initCalendarReader();

export const getCalendarData = () => calendarData;

export default {
  getCalendarData,
  sendRecruitmentGmailConfirmation,
  sendRegistrationGmailConfirmation,
  uploadToProgramSpreadsheets,
  createProgramCalendarEvent,
};
