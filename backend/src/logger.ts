import chalk from 'chalk';
import { logLinesCounter } from './prom-metrics';

enum LOG_LEVEL {
  INFO_LEVEL = 0,
  WARNING_LEVEL = 1,
  ERROR_LEVEL = 2
}

const colorByLevel = (level: LOG_LEVEL) => {
  if (level === LOG_LEVEL.INFO_LEVEL) return chalk.blueBright;
  if (level === LOG_LEVEL.WARNING_LEVEL) return chalk.yellow;
  if (level === LOG_LEVEL.ERROR_LEVEL) return chalk.red;
  return (a: any) => a;
};

const print = (msg: string, level: LOG_LEVEL) => {
  const stamp = new Date(Date.now());
  const color = colorByLevel(level);
  const coloredMsg = color(msg);
  console.log(`${stamp} ${coloredMsg}`);
};

const info = (msg: string) => {
  print(`INFO ${JSON.stringify(msg)}`, LOG_LEVEL.INFO_LEVEL);
  logLinesCounter.labels('info').inc();
};

const warning = (msg: string) => {
  print(`WARN ${JSON.stringify(msg)}`, LOG_LEVEL.WARNING_LEVEL);
  logLinesCounter.labels('warn').inc();
};

const error = (msg: string) => {
  print(`ERROR ${JSON.stringify(msg)}`, LOG_LEVEL.ERROR_LEVEL);
  logLinesCounter.labels('error').inc();
};

export default {
  info,
  warning,
  error,
};
