import models from './models';

export type SongData = {
  timestamp?: string;
  title?: string;
  artist?: string;
  duration?: number;
}

export type ProgrammeData = {
  id: string;
  title: string;
  description: string;
  start: string;
  end: string;
  team: string;
  genre: string;
  image: string;
}

// Get initial value for currently playing song

const getCurrentSongFromDb = async (): Promise<SongData> => {
  const results = await models.nowPlaying.find({}, {
    limit: 1,
    sort: {
      timestamp: -1,
    },
  });

  if (results.length === 0) {
    return {};
  }

  const current = results[0];

  if (new Date().valueOf() - Date.parse(current.timestamp) <= current.duration * 1000) {
    return {
      title: current.title,
      artist: current.artist,
      timestamp: current.timestamp,
      duration: current.duration,
    };
  }

  return {};
};

// Calculate the remaining time for the current song

const calculateSongRemainingTime = (songData: SongData): number | null => {
  if (songData.duration && songData.timestamp) {
    // eslint-disable-next-line max-len
    const remaining: number = songData.duration * 1000 - (new Date().valueOf() - Date.parse(songData.timestamp));
    if (remaining > 0) return remaining;
  }
  return null;
};

// Parse current programme from calendar data

const parseNowPlaying = (data: ProgrammeData[]): object => {
  const now: number = new Date().valueOf();
  const past: ProgrammeData[] = data.filter(d => Date.parse(d.start) < now);
  const current: ProgrammeData | undefined = past[past.length - 1];
  if (!current || Date.parse(current.end) < now) return {};
  return current;
};

export default {
  getCurrentSongFromDb,
  calculateSongRemainingTime,
  parseNowPlaying,
};
