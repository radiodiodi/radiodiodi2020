const notEmptyField = (name: string, val: any) => {
  if (!val) {
    throw new Error(`${name} cannot be null.`);
  }
};

export const validateRegistrationOrThrow = (data: any) => {
  notEmptyField('Name', data.name);
  notEmptyField('Email', data.email);
  notEmptyField('Description', data.description);
  notEmptyField('Genre', data.genre);
  notEmptyField('Duration', data.duration);
  notEmptyField('Participants', data.participants);
  notEmptyField('Producer', data.producer);
  notEmptyField('Prerecord', data.prerecord);
  // notEmptyField('Photoshoot', data.photoshoot);
  notEmptyField('Responsible', data.responsible);
  notEmptyField('Team', data.team);
  if (data.prerecord === 'live') {
    notEmptyField('Time propositions', data.propositions);
    if (!data.propositions.length || data.propositions.length === 0) {
      throw new Error('Time propositions are empty.');
    }
  }
  return true;
};

export const validateRecruitmentOrThrow = (data: any) => {
  notEmptyField('Name', data.name);
  notEmptyField('Email', data.email);
  notEmptyField('Message', data.message);
  return true;
};
