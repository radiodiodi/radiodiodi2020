import monk from 'monk';
import logger from './logger';
import environment from './environment';

const statsDatabaseURL = `${environment.MONGODB_HOST}/${environment.MONGODB_STATS_DB}`;
const shoutboxDatabaseURL = `${environment.MONGODB_HOST}/${environment.MONGODB_SHOUTBOX_DB}`;
const registrationsDatabaseURL = `${environment.MONGODB_HOST}/${environment.MONGODB_REGISTRATION_DB}`;
const songDatabaseURL = `${environment.MONGODB_HOST}/${environment.MONGODB_SONG_DB}`;

logger.info(`Mongo stats DB: ${statsDatabaseURL}`);
const statsDB = monk(statsDatabaseURL);
statsDB.then(() => {
  logger.info('Statistics database connected successfully.');
}).catch(err => {
  logger.error(err);
});

const listeners = statsDB.get('listeners');
const nowPlaying = statsDB.get('now_playing');

logger.info(`Mongo shoutbox DB: ${shoutboxDatabaseURL}`);
const shoutboxDB = monk(shoutboxDatabaseURL);
shoutboxDB.then(() => {
  logger.info('Shoutbox database connected successfully.');
}).catch(err => {
  logger.error(err);
});

const messages = shoutboxDB.get('messages');
const bans = shoutboxDB.get('bans');
const reserved = shoutboxDB.get('reserved');
const bannedWords = shoutboxDB.get('banned_words');
const filters = shoutboxDB.get('filters');
filters.createIndex('slug');

logger.info(`Mongo song DB: ${songDatabaseURL}`);
const songDB = monk(songDatabaseURL);
songDB.then(() => {
  logger.info('Song database connected successfully.');
}).catch(err => {
  logger.error(err);
});

const songs = songDB.get('songs');

logger.info(`Mongo registration DB: ${registrationsDatabaseURL}`);
const registrationDB = monk(registrationsDatabaseURL);
registrationDB.then(() => {
  logger.info('Registration database connected successfully.');
}).catch(err => {
  logger.error(err);
});

const registrations = registrationDB.get('registrations');
const recruitment = registrationDB.get('recruitment');

export default {
  listeners,
  nowPlaying,
  messages,
  bans,
  reserved,
  registrations,
  recruitment,
  bannedWords,
  filters,
  songs,
};
