const validateSongData = (songs : any) => {
  try {
    for (const song of songs) {
      // return false if any of the values is not
      // of type string
      if (typeof song.artist !== 'string') {
        return false;
      }
      if (typeof song.album !== 'string') {
        return false;
      }
      if (typeof song.title !== 'string') {
        return false;
      }
    }

    // all the values are strings, return true
    return true;
  } catch (e) {
    return false;
  }
};

export default validateSongData;
