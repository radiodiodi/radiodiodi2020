import axios from 'axios';
import { Middleware } from 'koa';
import Router from 'koa-router';
import { transliterate } from 'transliteration';
import { register } from 'prom-client';
import models from './models';
import logger from './logger';
import utils from './utils';
import websockets from './websockets';
import environment from './environment';
import {
  sendRegistrationGmailConfirmation,
  sendRecruitmentGmailConfirmation,
  getCalendarData,
  uploadToProgramSpreadsheets,
  createProgramCalendarEvent,
} from './googleUtils';
import { validateRegistrationOrThrow, validateRecruitmentOrThrow } from './validation';
import validateSongData from './songs';

const router = new Router();

// import { adsense } from 'googleapis/build/src/apis/adsense';

const admin = new Router();

const allowAllCors: Middleware = async (ctx, next) => {
  await next();
  ctx.set('Access-Control-Allow-Origin', '*');
};

const verifyUrl = (token: string) => `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${token}`;
const checkAuthorization: Middleware = async (ctx, next) => {
  const auth = ctx.headers.authorization;

  if (auth === undefined) {
    ctx.throw(400, 'No authorization header present.');
    return;
  }

  const url = verifyUrl(auth);
  const resp = await axios.get(url);

  const { hd, error_description } = resp.data;

  if (error_description) {
    ctx.throw(401, JSON.stringify({
      error: error_description,
    }));
    return;
  }

  if (hd !== 'radiodiodi.fi') {
    ctx.throw(401, JSON.stringify({
      error: 'Not a radiodiodi.fi email.',
    }));
    return;
  }

  await next();
};

admin.get('/registrations', async ctx => {
  const registrations = await models.registrations.find({}, { sort: { timestamp: 1 } });
  ctx.type = 'application/json';
  ctx.body = JSON.stringify(registrations);
});

admin.get('/messages', async ctx => {
  try {
    const messages = (await models.messages.find({}, {
      limit: 300, sort: { timestamp: -1 },
    })).reverse();

    ctx.body = JSON.stringify({
      messages,
    });
    ctx.type = 'application/json';
  } catch (error) {
    console.log(error);
    ctx.throw(500, 'Internal Server Error');
  }
});

admin.get('/words', async ctx => {
  try {
    const words = await models.bannedWords.find({});
    ctx.body = JSON.stringify({ words });
    ctx.type = 'application/json';
    ctx.status = 200;
  } catch (error) {
    logger.error(error);
    ctx.throw(500, 'Internal Server Error');
  }
});

admin.post('/words/:word', async ctx => {
  try {
    const { word } = ctx.params;
    if (typeof (word) !== 'string' || word.length < 4) {
      ctx.throw(400, JSON.stringify({
        error: 'Word must be at least 3 characters long.',
      }));
      return;
    }

    const existing = await models.bannedWords.findOne({ word });
    if (existing) {
      ctx.throw(400, JSON.stringify({
        error: 'Word already banned.',
      }));
      return;
    }

    const wordInDB = await models.bannedWords.insert({ word });
    ctx.body = JSON.stringify({ word: wordInDB });
    ctx.type = 'application/json';
    ctx.status = 201;
  } catch (error) {
    logger.error(error);
    ctx.throw(500, 'Internal Server Error');
  }
});

admin.delete('/words/remove/:id', async ctx => {
  try {
    const { id } = ctx.params;
    models.bannedWords.remove({ _id: id });
    ctx.status = 204;
  } catch (error) {
    logger.error(error);
    ctx.throw(500, 'Internal Server Error');
  }
});

admin.get('/filters', async ctx => {
  try {
    const filters = await models.filters.find({}, { sort: { order: 1 } });
    ctx.body = JSON.stringify({ filters });
    ctx.type = 'application/json';
  } catch (error) {
    logger.error(error);
    ctx.throw(500, 'Internal Server Error');
  }
});

admin.post('/filters/:slug/:state', async ctx => {
  const { slug, state } = ctx.params;
  if (state !== 'on' && state !== 'off') {
    ctx.throw(400, `State must be either 'on' or 'off', was: ${state}`);
    return;
  }
  try {
    if (await models.filters.count({ slug }) === 0) {
      ctx.throw(400, 'Slug not found in database');
      return;
    }
    const updated = await models.filters.findOneAndUpdate(
      { slug },
      { $set: { state: state === 'on' } },
      { upsert: true, returnOriginal: false },
    );
    ctx.body = JSON.stringify(updated);
    ctx.type = 'application/json';
  } catch (error) {
    logger.error(error);
    ctx.throw(500, 'Internal Server Error');
  }
});

admin.delete('/messages/remove/:id', async ctx => {
  try {
    const { id } = ctx.params;
    models.messages.remove({ _id: id });
    websockets.eraseMessage(id);
    ctx.status = 200;
  } catch (err) {
    logger.error(err);
    ctx.throw(500, err);
  }
});

admin.delete('/users/ban/:id', async ctx => {
  try {
    const { id } = ctx.params;
    const message = await models.messages.findOne({ _id: id });

    const { ip } = message;
    const existingBan = await models.bans.findOne({ ip });
    if (existingBan) {
      ctx.throw(400, JSON.stringify({
        error: 'User already banned.',
      }));
      return;
    }

    models.bans.insert({
      ip: message.ip,
      name: message.name,
      timeOfBan: new Date(Date.now()),
      timeOfMessage: message.timestamp,
      text: message.text,
    });
    ctx.status = 200;
  } catch (err) {
    logger.error(err);
    ctx.throw(500, err);
  }
});

admin.delete('/users/unban/:ip', async ctx => {
  try {
    const { ip } = ctx.params;
    const existingBan = await models.bans.findOne({ ip });

    if (!existingBan) {
      ctx.throw(400, JSON.stringify({
        error: 'User not banned.',
      }));
      return;
    }

    models.bans.remove({ ip });
    ctx.status = 200;
  } catch (err) {
    logger.error(err);
    ctx.throw(500, err);
  }
});

admin.get('/users/banned', async ctx => {
  const bans = await models.bans.find({}, {
    sort: { timestamp: 1 },
  });

  ctx.body = JSON.stringify({
    bans,
  });
  ctx.type = 'application/json';
});

admin.get('/users/reserved', async ctx => {
  const reserved = await models.reserved.find({});

  ctx.body = JSON.stringify({
    reserved,
  });
  ctx.type = 'application/json';
});

router.use('/admin', checkAuthorization, admin.routes(), admin.allowedMethods());

router.get('/', ctx => {
  ctx.body = 'Radiodiodi JSON API';
});

router.get('/auth', checkAuthorization, async ctx => {
  ctx.body = '';
  ctx.status = 200;
});

router.get('/stats', async ctx => {
  ctx.body = 'Not implemented';
});

router.get('/metrics', allowAllCors, async ctx => {
  ctx.body = await register.metrics();
  ctx.type = register.contentType;
});

router.get('/inspirational-quote', allowAllCors, ctx => {
  ctx.body = JSON.stringify({
    quote: 'Kukkakaalia - kakkakuulia: hauska munansaannos',
  });
  ctx.type = 'application/json';
});

router.get('/programmes', allowAllCors, ctx => {
  const data = getCalendarData();
  ctx.body = JSON.stringify(data);
  ctx.type = 'application/json';
});

router.get('/now_playing', allowAllCors, ctx => {
  const data = getCalendarData();
  if (!data) {
    ctx.status = 500;
    return;
  }
  const show = utils.parseNowPlaying(data);
  ctx.body = JSON.stringify(show);
  ctx.type = 'application/json';
});

router.get('/now_playing_ascii', allowAllCors, ctx => {
  const data = getCalendarData();
  if (!data) {
    ctx.status = 500;
    return;
  }
  const show = utils.parseNowPlaying(data);
  ctx.body = transliterate(JSON.stringify(show));
  ctx.type = 'application/json';
});

router.post('/api/register', async ctx => {
  const data = ctx.request.body;
  data.timestamp = new Date();

  console.log(data);
  try {
    validateRegistrationOrThrow(data);
  } catch (err) {
    ctx.body = String(err);
    ctx.status = 400;
    return;
  }

  try {
    await models.registrations.insert(data);
    logger.info(`Saved registration with name "${data.name}" to DB.`);

    await sendRegistrationGmailConfirmation(data);
    await uploadToProgramSpreadsheets(data);
    await createProgramCalendarEvent(data);
  } catch (err) {
    console.error(err);
    ctx.throw(500);
  }

  ctx.status = 200;
});

router.post('/api/recruitment', async ctx => {
  const data = ctx.request.body;
  data.timestamp = new Date();

  console.log(data);
  try {
    validateRecruitmentOrThrow(data);
  } catch (err) {
    ctx.body = String(err);
    ctx.status = 400;
    return;
  }

  try {
    await models.recruitment.insert(data);
    logger.info(`Saved recruitment application with name "${data.name}" to DB.`);

    await sendRecruitmentGmailConfirmation(data);
  } catch (err) {
    console.error(err);
    ctx.throw(500);
  }

  ctx.status = 200;
});

router.post('/api/update_current_song', async ctx => {
  let auth, title, artist, duration;
  try {
    const unparsed = Symbol.for('unparsedBody');
    const data = ctx.request.body[unparsed];
    const splitted = data.split('||penis||');
    [auth, title, artist, duration] = splitted;

    if (auth !== environment.RADIODJ2_AUTH_SECRET) {
      logger.error(`Bad current song request: ${String(data)}`);
      ctx.throw(401, 'Unauthorized');
      return;
    }
    if (!title || !artist) {
      throw new Error(`Invalid title or artist. Request body: ${JSON.stringify(data)}`);
    }
  } catch (error) {
    console.log(error);
    ctx.throw(400, String(error));
    return;
  }

  if (artist.toLowerCase().startsWith('radiodiodi')) {
    ctx.body = JSON.stringify({
      status: 'skipped',
    });
    return;
  }

  try {
    const result = await models.nowPlaying.insert({
      timestamp: new Date(),
      title: String(title),
      artist: String(artist),
      duration: duration || 180, // default 3min if no duration from RadioDJ
    });
    websockets.updateSong(result);
    logger.info(`Updated current song: ${artist} - ${title}`);
  } catch (error) {
    console.log(error);
    ctx.throw(500, 'Internal server error.');
    return;
  }

  ctx.body = JSON.stringify({
    status: 'ok',
  });
  ctx.type = 'application/json';
});

/* DEPRECATED: GET /api/current_song
 * Current implementation for updating current song in frontend is implemented
 * with WebSockets. The route still remains here for debugging/testing purposes.
 */

router.get('/api/current_song', async ctx => {
  ctx.body = await utils.getCurrentSongFromDb();
  ctx.type = 'application/json';
});

type InstagramChildObject = {
    id: string,
    media_url?: string,
    media_type?: string,
    permalink?: string,
}

type InstagramObject = {
  media_url: string;
  permalink: string,
  caption: string,
  id: string,
  media_type: string,
  children?: { data: InstagramChildObject[]},
}

type InstagramResponse = {
  data: InstagramObject[]
};

interface InstagramCache {
  payload: InstagramObject[] | null;
  refreshed: number | null;
}

const instagramInMemoryCache: InstagramCache = {
  payload: null,
  refreshed: null,
};

router.get('/api/instagram_feed', allowAllCors, async ctx => {
  const { count } = ctx.query;
  const userId = environment.INSTAGRAM_USER_ID;
  const accessToken = environment.INSTAGRAM_ACCESS_TOKEN;
  const fields = [
    'children',
    'caption',
    'media_type',
    'media_url',
    'permalink',
    'timestamp',
    // 'like_count',
  ].join(',');

  // Refresh API token, it lives 60 days
  try {
    axios.get(`https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=${accessToken}`);
  } catch (err) {
    logger.error('Failed to refresh Instagram token');
    console.error(err);
  }

  const url = `https://graph.instagram.com/${userId}/media?fields=${fields}&access_token=${accessToken}&limit=${count}`;
  const cacheMaxAge = 60 * 60; // 1 hour
  const inMemoryCacheAge = 5 * 60; // 1 minute

  const now = new Date().valueOf();
  const { refreshed } = instagramInMemoryCache;
  // eslint-disable-next-line max-len
  const isCacheStale = !refreshed || now - refreshed > inMemoryCacheAge * 1000;

  let responseBody;
  if (isCacheStale) {
    logger.info('Instagram cache stale, fetching new from IG API');
    // if cache stale, fetch new data
    // otherwise respond with data from in memory cache
    try {
      const resp = await axios.get<InstagramResponse>(url);
      const { data }: InstagramResponse = resp.data;

      for (const post of data) {
        if (post.media_type === 'CAROUSEL_ALBUM' && post.children) {
          const promises = post.children.data.map((child) => {
            const child_url = `https://graph.instagram.com/${child.id}?fields=media_url,media_type,permalink&access_token=${accessToken}`;
            return axios.get(child_url);
          });
          const response = await Promise.all(promises);
          const children: InstagramChildObject[] = response.map(res => res.data);
          post.children.data = children;
        }
      }

      // update cache
      instagramInMemoryCache.payload = data;
      instagramInMemoryCache.refreshed = now;
      responseBody = data;
    } catch (err) {
      // if IG API responds with error, use last cache
      logger.error('Failed to get Instagram feed data, using latest cache');
      console.log(err);
      responseBody = instagramInMemoryCache.payload;
    }
  } else {
    logger.info('Instagram cache fresh, using cache');
    // if cache fresh, use cached response
    responseBody = instagramInMemoryCache.payload;
  }

  ctx.set('Cache-Control', `max-age=${cacheMaxAge}`);
  ctx.body = JSON.stringify({
    data: responseBody,
  });
  ctx.type = 'application/json';
});

type LibraryResult = {
  title: string;
  artist: string,
  album: string,
}

const paginateData = (data: LibraryResult[], page: number): LibraryResult[] => {
  const ROWS_PER_PAGE = 50;

  const start = ((page) - 1) * ROWS_PER_PAGE;
  const end = page * ROWS_PER_PAGE;
  if (start > data.length) {
    return [];
  }
  return data.slice(start, end);
};

router.get('/library/search', async ctx => {
  const { type, search, page } = ctx.query;
  const sanitizedSearch = search.replace(/\$/g, '');
  logger.info(`Querying for ${type} "${sanitizedSearch}".`);

  const query = new RegExp(sanitizedSearch, 'i');
  const queryObj = Object.fromEntries([[type, query]]);

  const result: LibraryResult[] = await models.songs.find(queryObj);
  const pageResult = paginateData(result, page);

  ctx.body = JSON.stringify({ result: pageResult, amount: result.length });
  ctx.type = 'application/json';
  ctx.status = 200;
});

// updates the song metadata collection, expects
// a list of songs
router.post('/library/songs', async ctx => {
  const auth = ctx.headers.authorization;

  if (auth === undefined || auth !== process.env.SONG_UPDATE_TOKEN) {
    ctx.body = JSON.stringify('Unauthorized');
    ctx.type = 'application/json';
    ctx.throw(403);
  }

  const data = ctx.request.body;
  const isValidSongData = validateSongData(data);

  if (!isValidSongData) {
    ctx.body = JSON.stringify('Invalid song data');
    ctx.type = 'application/json';
    ctx.throw(401);
  }

  try {
    // remove old songs and replace them with new ones
    await models.songs.remove({});
    await models.songs.insert(data);
    logger.info('Inserted new songs to the DB');
  } catch (err) {
    console.error(err);
    ctx.throw(500);
  }

  ctx.type = 'application/json';
  ctx.status = 200;
});

export default router;
