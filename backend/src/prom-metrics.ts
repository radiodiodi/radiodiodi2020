import client from 'prom-client';

export const emailSentCounter = new client.Counter({
  name: 'diodi_email_sent_total',
  help: 'Number of emails sent since restart. Increases on errors too',
});
export const emailErrorCounter = new client.Counter({
  name: 'diodi_email_errors_total',
  help: 'Number of email send failures since restart',
});
export const messagesReceivedCounter = new client.Counter({
  name: 'diodi_ws_messages_received_total',
  help: 'Number of chat messages received since restart',
});
export const messagesRelayedHistogram = new client.Histogram({
  name: 'diodi_ws_message_processing_time_seconds',
  help: 'Histogram of processing times of successfully processed messages',
});
export const messagesDroppedCounter = new client.Counter({
  name: 'diodi_ws_messages_dropped',
  help: 'Number of chat messages dropped for reason since restart',
  labelNames: ['reason'],
});
export const wsThrotleIncidents = new client.Counter({
  name: 'diodi_ws_throtle_incidents_total',
  help: 'Number of times client has been told to cool down',
});
export const wsCurrentClients = new client.Gauge({
  name: 'diodi_ws_clients',
  help: 'Number of ws clients. Updated every 10 seconds',
});
export const wsErrorsCounter = new client.Counter({
  name: 'diodi_ws_errors_total',
  help: 'Number of ws errors since restart',
});
export const logLinesCounter = new client.Counter({
  name: 'diodi_log_lines_total',
  help: 'Number of lines logged. Labeled by severity',
  labelNames: ['severity'],
});

export default {
  emailSentCounter,
  emailErrorCounter,
  messagesReceivedCounter,
  messagesRelayedHistogram,
  messagesDroppedCounter,
  wsThrotleIncidents,
  wsCurrentClients,
  wsErrorsCounter,
  logLinesCounter,
};
