declare module 'ws-rate-limit' {
  import WebSocket from 'ws';

  type limiter = (client: WebSocket) => void;
  // css-duration
  function ratelimiter(duration: string, max: number): limiter;
  export default ratelimiter;

}
